# Hamilton

Hamilton math is library for 3D graphics

## Vector

There are only `vec2<T>`, `vec3<T>`, and `vec4<T>`

```C++
vec3f v { 1.5f, 2.5f, 3.5f };
v += 1;
v -= 2;
vec3f nv = v * vec3f { 1.5f, -2.5f, 4.7f };
vec4f v4 { nv.x, nv.y, nv.z, 1.0f };
```

## Matrix

There are only `mat3<T>`, and `mat4<T>`
Note that matrices is column major

```C++
//our matrices are column major
mat3f A { 1.0f, 2.0f, 3.0f,		//first column
		  4.0f, 5.0f, 6.0f,		//second column
		  7.0f, 8.0f, 9.0f };	//third column

//our matrices are column major
mat3f B { 10.0f, 11.0f, 12.0f,		//first column
		  13.0f, 14.0f, 15.0f,		//second column
		  16.0f, 17.0f, 18.0f };	//third column

A += 1.0f;
printfmt("{}\n", A[0] == vec3f { 2.0f, 3.0f, 4.0f });

mat3f C = A * B;
```

## Quaternion

Let's rotate some vector by a quaternion

```C++
//quat from axis takes an axis to rotate around and an angle in radians
quatf q = quat_from_axis(vec3f { 0.0f, 0.0f, 1.0f }, PI/2);
vec4f v { 1.0f, 0.0f, 0.0f, 0.0f };
//we can convert the quaternion to a matrix and then use it
vec4f a = quat_to_mat(q) * v;
//or we can use the quaternion directly which is better
vec4f b = q * v;
printfmt("{}\n", a == b);
```