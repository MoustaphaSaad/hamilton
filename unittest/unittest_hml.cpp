#include <catch2/catch.hpp>

#include <hml/Vector.h>
#include <hml/Matrix.h>
#include <hml/Quaternion.h>

using namespace hml;

TEST_CASE("vec2", "[hml]")
{
	SECTION("Case 01")
	{
		vec2f a = vec2_splat<float>(0), b = vec2_splat<float>(1);

		CHECK(a.x == 0);
		CHECK(a.y == 0);

		CHECK(b.y == 1);
		CHECK(b.x == 1);
	}

	SECTION("Case 02")
	{
		vec2f a { 1.23, 65.234 };
		CHECK(a.x == 1.23f);
		CHECK(a.y == 65.234f);
	}
}

TEST_CASE("vec2 add", "[hml]")
{
	SECTION("Case 01")
	{
		vec2f a{};
		a += 1;
		CHECK(a == vec2_splat<float>(1));

		vec2f b = a + vec2f { 10, 20 };
		CHECK(b == vec2f { 11, 21 });
	}
}

TEST_CASE("vec2 sub", "[hml]")
{
	SECTION("Case 01")
	{
		vec2f a{};
		a -= 1;
		CHECK(a == vec2_splat<float>(-1));

		vec2f b = a - vec2f { 10, 20 };
		CHECK(b == vec2f { -11, -21 });
	}
}

TEST_CASE("vec2 mul", "[hml]")
{
	SECTION("Case 01")
	{
		vec2f a{};
		a *= 1;
		CHECK(a == vec2_splat<float>(0));

		vec2f b = vec2_splat<float>(1) * vec2f { 10, 20 };
		CHECK(b == vec2f { 10, 20 });

		b *= 0.5;
		CHECK(b == vec2f { 5, 10 });
	}
}

TEST_CASE("vec2 div", "[hml]")
{
	SECTION("Case 01")
	{
		vec2f a{};
		a /= 1;
		CHECK(a == vec2_splat<float>(0));

		vec2f b = vec2_splat<float>(1) / vec2f { 10, 20 };
		CHECK(b.x == Approx(1.0/10.0));
		CHECK(b.y == Approx(1.0/20.0));

		b *= 2;
		CHECK(b.x == Approx(1.0/5.0));
		CHECK(b.y == Approx(1.0/10.0));
	}
}

TEST_CASE("vec2 mod", "[hml]")
{
	SECTION("Case 01")
	{
		vec2i a{};
		a %= 1;
		CHECK(a == vec2_splat<int>(0));

		vec2i b = vec2_splat<int>(1) % vec2i { 10, 20 };
		CHECK(b.x == 1);
		CHECK(b.y == 1);

		vec2i c {9, 8};
		c %= 5;
		CHECK(c.x == 4);
		CHECK(c.y == 3);
	}
}

TEST_CASE("vec2 unary op", "[hml]")
{
	SECTION("Case 01")
	{
		vec2f a { 1, 2.324 };
		vec2f b = -a;
		CHECK(b.x == -1.0f);
		CHECK(b.y == -2.324f);
	}
}

TEST_CASE("vec2 sqrt", "[hml]")
{
	SECTION("Case 01")
	{
		vec2f a { 4, 16 };
		a = sqrt(a);
		CHECK(a.x == 2);
		CHECK(a.y == 4);
	}
}

TEST_CASE("vec2 dot", "hml")
{
	SECTION("Case 01")
	{
		vec2f a{1, 2};
		vec2f b{3, 4};
		float c = dot(a, b);

		CHECK(c == a.x * b.x + a.y * b.y);
	}
}

TEST_CASE("vec2 clamp", "hml")
{
	SECTION("Case 01")
	{
		vec2f a{2.5f, -1.0f};
		vec2f b = clamp(a, 1.5f, 10.0f);

		CHECK(b.x <= 10.0f);
		CHECK(b.x >= 1.5f);

		CHECK(b.y <= 10.0f);
		CHECK(b.y >= 1.5f);
	}

	SECTION("Case 02")
	{
		vec2f a{2.5f, -1.0f};
		vec2f b = clamp(a, vec2f { 1.5f, 10.0f }, vec2f { 20.0f, 11.0f });

		CHECK(b.x <= 20.0f);
		CHECK(b.x >= 1.5f);

		CHECK(b.y <= 11.0f);
		CHECK(b.y >= 10.0f);
	}
}

TEST_CASE("vec2 length", "hml")
{
	SECTION("Case 01")
	{
		vec2f a{2.5f, -1.0f};
		float result = length_squared(a);

		CHECK(result == 7.25f);
	}

	SECTION("Case 02")
	{
		vec2f a{2.5f, -1.0f};
		float result = length(a);

		CHECK(result == 2.69258240f);
	}
}

TEST_CASE("vec2 normalize", "hml")
{
	SECTION("Case 01")
	{
		vec2f a{2.5f, -1.0f};
		a = normalize(a);

		CHECK(length(a) == 1.0f);
	}
}

TEST_CASE("vec2 min", "hml")
{
	SECTION("Case 01")
	{
		vec2f a{2.5f, -1.0f};
		CHECK(min(a) == -1.0f);
	}

	SECTION("Case 02")
	{
		vec2f a{2.5f, -1.0f};
		vec2f b{1.4f, 10.0f};
		CHECK(min(a, b) == vec2f{1.4f, -1.0f});
	}
}

TEST_CASE("vec2 max", "hml")
{
	SECTION("Case 01")
	{
		vec2f a{2.5f, -1.0f};
		CHECK(max(a) == 2.5f);
	}

	SECTION("Case 02")
	{
		vec2f a{2.5f, -1.0f};
		vec2f b{1.4f, 10.0f};
		CHECK(max(a, b) == vec2f{2.5f, 10.0f});
	}
}

TEST_CASE("vec2 abs", "hml")
{
	SECTION("Case 01")
	{
		vec2f a{2.5f, -1.0f};
		CHECK(abs(a) == vec2f{2.5f, 1.0f});
	}
}

TEST_CASE("vec2 sum", "hml")
{
	SECTION("Case 01")
	{
		vec2f a{2.5f, -1.0f};
		CHECK(sum(a) == 1.5f);
	}
}


TEST_CASE("vec3", "[hml]")
{
	SECTION("Case 01")
	{
		vec3f a = vec3_splat<float>(0), b = vec3_splat<float>(1);

		CHECK(a.x == 0);
		CHECK(a.y == 0);
		CHECK(a.z == 0);

		CHECK(b.y == 1);
		CHECK(b.x == 1);
		CHECK(b.z == 1);
	}

	SECTION("Case 02")
	{
		vec3f a { 1.23, 65.234, 1234 };
		CHECK(a.x == 1.23f);
		CHECK(a.y == 65.234f);
		CHECK(a.z == 1234.0f);
	}
}

TEST_CASE("vec3 add", "[hml]")
{
	SECTION("Case 01")
	{
		vec3f a{};
		a += 1;
		CHECK(a == vec3_splat<float>(1));

		vec3f b = a + vec3f { 10, 20, 30 };
		CHECK(b == vec3f { 11, 21, 31 });
	}
}

TEST_CASE("vec3 sub", "[hml]")
{
	SECTION("Case 01")
	{
		vec3f a{};
		a -= 1;
		CHECK(a == vec3_splat<float>(-1));

		vec3f b = a - vec3f { 10, 20, 30 };
		CHECK(b == vec3f { -11, -21, -31 });
	}
}

TEST_CASE("vec3 mul", "[hml]")
{
	SECTION("Case 01")
	{
		vec3f a{};
		a *= 1;
		CHECK(a == vec3_splat<float>(0));

		vec3f b = vec3_splat<float>(1) * vec3f { 10, 20, 30 };
		CHECK(b == vec3f { 10, 20, 30 });

		b *= 0.5;
		CHECK(b == vec3f { 5, 10, 15 });
	}
}

TEST_CASE("vec3 div", "[hml]")
{
	SECTION("Case 01")
	{
		vec3f a{};
		a /= 1;
		CHECK(a == vec3_splat<float>(0));

		vec3f b = vec3_splat<float>(1) / vec3f { 10, 20, 30 };
		CHECK(b.x == Approx(1.0/10.0));
		CHECK(b.y == Approx(1.0/20.0));
		CHECK(b.z == Approx(1.0/30.0));

		b *= 2;
		CHECK(b.x == Approx(1.0/5.0));
		CHECK(b.y == Approx(1.0/10.0));
		CHECK(b.z == Approx(1.0/15.0));
	}
}

TEST_CASE("vec3 mod", "[hml]")
{
	SECTION("Case 01")
	{
		vec3i a{};
		a %= 1;
		CHECK(a == vec3_splat<int>(0));

		vec3i b = vec3_splat<int>(1) % vec3i { 10, 20, 30 };
		CHECK(b.x == 1);
		CHECK(b.y == 1);
		CHECK(b.z == 1);

		vec3i c {9, 8, 7};
		c %= 5;
		CHECK(c.x == 4);
		CHECK(c.y == 3);
		CHECK(c.z == 2);
	}
}

TEST_CASE("vec3 unary op", "[hml]")
{
	SECTION("Case 01")
	{
		vec3f a { 1, 2.324, 123 };
		vec3f b = -a;
		CHECK(b.x == -1.0f);
		CHECK(b.y == -2.324f);
		CHECK(b.z == -123.0f);
	}
}

TEST_CASE("vec3 sqrt", "[hml]")
{
	SECTION("Case 01")
	{
		vec3f a { 4, 16, 25 };
		a = sqrt(a);
		CHECK(a.x == 2);
		CHECK(a.y == 4);
		CHECK(a.z == 5);
	}
}

TEST_CASE("vec3 dot", "hml")
{
	SECTION("Case 01")
	{
		vec3f a{1, 2, 3};
		vec3f b{4, 5, 6};
		float c = dot(a, b);

		CHECK(c == a.x * b.x + a.y * b.y + a.z * b.z);
	}
}

TEST_CASE("vec3 clamp", "hml")
{
	SECTION("Case 01")
	{
		vec3f a{2.5f, -1.0f, 11.0f};
		vec3f b = clamp(a, 1.5f, 10.0f);

		CHECK(b.x <= 10.0f);
		CHECK(b.x >= 1.5f);

		CHECK(b.y <= 10.0f);
		CHECK(b.y >= 1.5f);

		CHECK(b.z <= 10.0f);
		CHECK(b.z >= 1.5f);
	}

	SECTION("Case 02")
	{
		vec3f a{2.5f, -1.0f, 12.0f };
		vec3f b = clamp(a, vec3f { 1.5f, 10.0f, 1.0f }, vec3f { 20.0f, 11.0f, 2.0f });

		CHECK(b.x <= 20.0f);
		CHECK(b.x >= 1.5f);

		CHECK(b.y <= 11.0f);
		CHECK(b.y >= 10.0f);

		CHECK(b.z <= 2.0f);
		CHECK(b.z >= 1.0f);
	}
}

TEST_CASE("vec3 length", "hml")
{
	SECTION("Case 01")
	{
		vec3f a{2.5f, -1.0f, 2.0f};
		float result = length_squared(a);

		CHECK(result == 11.25f);
	}

	SECTION("Case 02")
	{
		vec3f a{2.5f, -1.0f, 2.0f};
		float result = length(a);

		CHECK(result == 3.354101966f);
	}
}

TEST_CASE("vec3 normalize", "hml")
{
	SECTION("Case 01")
	{
		vec3f a{2.5f, -1.0f, 2.0f};
		a = normalize(a);

		CHECK(length(a) == 1.0f);
	}
}

TEST_CASE("vec3 min", "hml")
{
	SECTION("Case 01")
	{
		vec3f a{2.5f, -1.0f, 1.5f};
		CHECK(min(a) == -1.0f);
	}

	SECTION("Case 02")
	{
		vec3f a{2.5f, -1.0f, 1.5f};
		vec3f b{1.4f, 10.0f, 1.6f};
		CHECK(min(a, b) == vec3f{1.4f, -1.0f, 1.5f});
	}
}

TEST_CASE("vec3 max", "hml")
{
	SECTION("Case 01")
	{
		vec3f a{2.5f, -1.0f, 1.5f};
		CHECK(max(a) == 2.5f);
	}

	SECTION("Case 02")
	{
		vec3f a{2.5f, -1.0f, 1.5f};
		vec3f b{1.4f, 10.0f, 1.6f};
		CHECK(max(a, b) == vec3f{2.5f, 10.0f, 1.6f});
	}
}

TEST_CASE("vec3 abs", "hml")
{
	SECTION("Case 01")
	{
		vec3f a{2.5f, -1.0f, 1.5f};
		CHECK(abs(a) == vec3f{2.5f, 1.0f, 1.5f});
	}
}

TEST_CASE("vec3 sum", "hml")
{
	SECTION("Case 01")
	{
		vec3f a{2.5f, -1.0f, 1.5f};
		CHECK(sum(a) == 3.0f);
	}
}

TEST_CASE("vec3 cross", "[hml]")
{
	SECTION("Case 01")
	{
		vec3f a{2.0f, 4.0f, 3.5f};
		vec3f b{5.0f, 8.0f, 6.2f};
		auto c = cross(a, b);

		CHECK(c[0] == a[1] * b[2] - b[1] * a[2]);
		CHECK(c[1] == b[0] * a[2] - a[0] * b[2]);
		CHECK(c[2] == a[0] * b[1] - b[0] * a[1]);
	}
}


TEST_CASE("vec4", "[hml]")
{
	SECTION("Case 01")
	{
		vec4f a = vec4_splat<float>(0), b = vec4_splat<float>(1);

		CHECK(a.x == 0);
		CHECK(a.y == 0);
		CHECK(a.z == 0);
		CHECK(a.w == 0);

		CHECK(b.y == 1);
		CHECK(b.x == 1);
		CHECK(b.z == 1);
		CHECK(b.w == 1);
	}

	SECTION("Case 02")
	{
		vec4f a { 1.23, 65.234, 1234, 1 };
		CHECK(a.x == 1.23f);
		CHECK(a.y == 65.234f);
		CHECK(a.z == 1234.0f);
		CHECK(a.w == 1.0f);
	}
}

TEST_CASE("vec4 add", "[hml]")
{
	SECTION("Case 01")
	{
		vec4f a{};
		a += 1;
		CHECK(a == vec4_splat<float>(1));

		vec4f b = a + vec4f { 10, 20, 30, 40 };
		CHECK(b == vec4f { 11, 21, 31, 41 });
	}
}

TEST_CASE("vec4 sub", "[hml]")
{
	SECTION("Case 01")
	{
		vec4f a{};
		a -= 1;
		CHECK(a == vec4_splat<float>(-1));

		vec4f b = a - vec4f { 10, 20, 30, 40 };
		CHECK(b == vec4f { -11, -21, -31, -41 });
	}
}

TEST_CASE("vec4 mul", "[hml]")
{
	SECTION("Case 01")
	{
		vec4f a{};
		a *= 1;
		CHECK(a == vec4_splat<float>(0));

		vec4f b = vec4_splat<float>(1) * vec4f { 10, 20, 30, 40 };
		CHECK(b == vec4f { 10, 20, 30, 40 });

		b *= 0.5;
		CHECK(b == vec4f { 5, 10, 15, 20 });
	}
}

TEST_CASE("vec4 div", "[hml]")
{
	SECTION("Case 01")
	{
		vec4f a{};
		a /= 1;
		CHECK(a == vec4_splat<float>(0));

		vec4f b = vec4_splat<float>(1) / vec4f { 10, 20, 30, 40 };
		CHECK(b.x == Approx(1.0/10.0));
		CHECK(b.y == Approx(1.0/20.0));
		CHECK(b.z == Approx(1.0/30.0));
		CHECK(b.w == Approx(1.0/40.0));

		b *= 2;
		CHECK(b.x == Approx(1.0/5.0));
		CHECK(b.y == Approx(1.0/10.0));
		CHECK(b.z == Approx(1.0/15.0));
		CHECK(b.w == Approx(1.0/20.0));
	}
}

TEST_CASE("vec4 mod", "[hml]")
{
	SECTION("Case 01")
	{
		vec4i a{};
		a %= 1;
		CHECK(a == vec4_splat<int>(0));

		vec4i b = vec4_splat<int>(1) % vec4i { 10, 20, 30, 40 };
		CHECK(b.x == 1);
		CHECK(b.y == 1);
		CHECK(b.z == 1);
		CHECK(b.w == 1);

		vec4i c {9, 8, 7, 6};
		c %= 5;
		CHECK(c.x == 4);
		CHECK(c.y == 3);
		CHECK(c.z == 2);
		CHECK(c.w == 1);
	}
}

TEST_CASE("vec4 unary op", "[hml]")
{
	SECTION("Case 01")
	{
		vec4f a { 1, 2.324, 123, 4 };
		vec4f b = -a;
		CHECK(b.x == -1.0f);
		CHECK(b.y == -2.324f);
		CHECK(b.z == -123.0f);
		CHECK(b.w == -4.0f);
	}
}

TEST_CASE("vec4 sqrt", "[hml]")
{
	SECTION("Case 01")
	{
		vec4f a { 4, 16, 25, 36 };
		a = sqrt(a);
		CHECK(a.x == 2);
		CHECK(a.y == 4);
		CHECK(a.z == 5);
		CHECK(a.w == 6);
	}
}

TEST_CASE("vec4 dot", "hml")
{
	SECTION("Case 01")
	{
		vec4f a{1, 2, 3, 4};
		vec4f b{5, 6, 7, 8};
		float c = dot(a, b);

		CHECK(c == a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w);
	}
}

TEST_CASE("vec4 clamp", "hml")
{
	SECTION("Case 01")
	{
		vec4f a{2.5f, -1.0f, 11.0f, 12.0f};
		vec4f b = clamp(a, 1.5f, 10.0f);

		CHECK(b.x <= 10.0f);
		CHECK(b.x >= 1.5f);

		CHECK(b.y <= 10.0f);
		CHECK(b.y >= 1.5f);

		CHECK(b.z <= 10.0f);
		CHECK(b.z >= 1.5f);

		CHECK(b.w <= 10.0f);
		CHECK(b.w >= 1.5f);
	}

	SECTION("Case 02")
	{
		vec4f a{2.5f, -1.0f, 12.0f, 10.0f };
		vec4f b = clamp(a, vec4f { 1.5f, 10.0f, 1.0f, -100.0f }, vec4f { 20.0f, 11.0f, 2.0f, 100.0f });

		CHECK(b.x <= 20.0f);
		CHECK(b.x >= 1.5f);

		CHECK(b.y <= 11.0f);
		CHECK(b.y >= 10.0f);

		CHECK(b.z <= 2.0f);
		CHECK(b.z >= 1.0f);

		CHECK(b.w <= 100.0f);
		CHECK(b.w >= -100.0f);
	}
}

TEST_CASE("vec4 length", "hml")
{
	SECTION("Case 01")
	{
		vec4f a{2.5f, -1.0f, 2.0f, 1.0f};
		float result = length_squared(a);

		CHECK(result == 12.25f);
	}

	SECTION("Case 02")
	{
		vec4f a{2.5f, -1.0f, 2.0f, 1.0f};
		float result = length(a);

		CHECK(result == 3.5f);
	}
}

TEST_CASE("vec4 normalize", "hml")
{
	SECTION("Case 01")
	{
		vec4f a{2.5f, -1.0f, 2.0f, 1.0f};
		a = normalize(a);

		CHECK(length(a) == 1.0f);
	}
}

TEST_CASE("vec4 min", "hml")
{
	SECTION("Case 01")
	{
		vec4f a{2.5f, -1.0f, 1.5f, 1.0f};
		CHECK(min(a) == -1.0f);
	}

	SECTION("Case 02")
	{
		vec4f a{2.5f, -1.0f, 1.5f, 1.0f};
		vec4f b{1.4f, 10.0f, 1.6f, 0.9f};
		CHECK(min(a, b) == vec4f{1.4f, -1.0f, 1.5f, 0.9f});
	}
}

TEST_CASE("vec4 max", "hml")
{
	SECTION("Case 01")
	{
		vec4f a{2.5f, -1.0f, 1.5f, 1.0f};
		CHECK(max(a) == 2.5f);
	}

	SECTION("Case 02")
	{
		vec4f a{2.5f, -1.0f, 1.5f, 1.0f};
		vec4f b{1.4f, 10.0f, 1.6f, 0.9f};
		CHECK(max(a, b) == vec4f{2.5f, 10.0f, 1.6f, 1.0f});
	}
}

TEST_CASE("vec4 abs", "hml")
{
	SECTION("Case 01")
	{
		vec4f a{2.5f, -1.0f, 1.5f, 0.9f};
		CHECK(abs(a) == vec4f{2.5f, 1.0f, 1.5f, 0.9f});
	}
}

TEST_CASE("vec4 sum", "hml")
{
	SECTION("Case 01")
	{
		vec4f a{2.5f, -1.0f, 1.5f, 0.75};
		CHECK(sum(a) == 3.75f);
	}
}


TEST_CASE("mat2 construction", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat2f a { 1.0f, 2.0f,		//first column
				  3.0f, 4.0f };		//second column
		CHECK(a[0] == vec2f{1.0f, 2.0f});
		CHECK(a[1] == vec2f{3.0f, 4.0f});
	}
}

TEST_CASE("mat2 add", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat2f a { 1.0f, 2.0f,		//first column
				  3.0f, 4.0f };		//second column

		a += 1.0f;
		CHECK(a[0] == vec2f{2.0f, 3.0f});
		CHECK(a[1] == vec2f{4.0f, 5.0f});

		a += vec2f { -1.0f, 2.0f };
		CHECK(a[0] == vec2f{1.0f, 5.0f});
		CHECK(a[1] == vec2f{3.0f, 7.0f});
	}
}

TEST_CASE("mat2 sub", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat2f a { 1.0f, 2.0f,		//first column
				  3.0f, 4.0f };		//second column

		a -= 1.0f;
		CHECK(a[0] == vec2f{0.0f, 1.0f});
		CHECK(a[1] == vec2f{2.0f, 3.0f});

		a -= vec2f { -1.0f, 2.0f };
		CHECK(a[0] == vec2f{1.0f, -1.0f});
		CHECK(a[1] == vec2f{3.0f, 1.0f});
	}
}

TEST_CASE("mat2 mul", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat2f a { 1.0f, 2.0f,		//first column
				  3.0f, 4.0f };		//second column

		//our matrices are column major
		mat2f b { 5.0f, 6.0f,		//first column
				  7.0f, 8.0f };		//second column

		mat2f c = a * b;
		CHECK(c[0] == vec2f { dot(a.row(0), b[0]), dot(a.row(1), b[0])});
		CHECK(c[1] == vec2f { dot(a.row(0), b[1]), dot(a.row(1), b[1])});
	}

	SECTION("Case 02")
	{
		//our matrices are column major
		mat2f a { 1.0f, 2.0f,		//first column
				  3.0f, 4.0f };		//second column
		
		vec2f b { 1.0f, 2.0f };
		vec2f c = a * b;
		CHECK(c.x == 7.0f);
		CHECK(c.y == 10.0f);
	}
}

TEST_CASE("mat2 transpose", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat2f a { 1.0f, 2.0f,		//first column
				  3.0f, 4.0f };		//second column
		mat2f b = transpose(a);
		CHECK(b[0] == vec2f{1.0f, 3.0f});
		CHECK(b[1] == vec2f{2.0f, 4.0f});
	}
}

TEST_CASE("mat2 determiniant", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat2f a { 1.0f, 2.0f,		//first column
				  3.0f, 4.0f };		//second column
		CHECK(determinant(a) == -2.0f);
		CHECK(invertible(a) == true);
	}
}

TEST_CASE("mat2 inverse", "hml")
{
	SECTION("Case 01")
	{
		mat2f a { 3.0f, -7.0f,
				  5.0f, 2.0f };
		mat2f b = inverse(a);
		CHECK(b[0][0] == Approx(2.0f/41.0f));
		CHECK(b[0][1] == Approx(7.0f/41.0f));
		CHECK(b[1][0] == Approx(-5.0f/41.0f));
		CHECK(b[1][1] == Approx(3.0f/41.0f));
	}
}


TEST_CASE("mat3 construction", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat3f a { 1.0f, 2.0f, 3.0f,		//first column
				  4.0f, 5.0f, 6.0f,		//second column
				  7.0f, 8.0f, 9.0f };	//third column
		CHECK(a[0] == vec3f{1.0f, 2.0f, 3.0f});
		CHECK(a[1] == vec3f{4.0f, 5.0f, 6.0f});
		CHECK(a[2] == vec3f{7.0f, 8.0f, 9.0f});
	}
}

TEST_CASE("mat3 add", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat3f a { 1.0f, 2.0f, 3.0f,		//first column
				  4.0f, 5.0f, 6.0f,		//second column
				  7.0f, 8.0f, 9.0f };	//third column

		a += 1.0f;
		CHECK(a[0] == vec3f{2.0f, 3.0f, 4.0f});
		CHECK(a[1] == vec3f{5.0f, 6.0f, 7.0f});
		CHECK(a[2] == vec3f{8.0f, 9.0f, 10.0f});

		a += vec3f { -1.0f, 2.0f, 0.0f };
		CHECK(a[0] == vec3f{1.0f, 5.0f, 4.0f});
		CHECK(a[1] == vec3f{4.0f, 8.0f, 7.0f});
		CHECK(a[2] == vec3f{7.0f, 11.0f, 10.0f});
	}
}

TEST_CASE("mat3 sub", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat3f a { 1.0f, 2.0f, 3.0f,		//first column
				  4.0f, 5.0f, 6.0f,		//second column
				  7.0f, 8.0f, 9.0f };	//third column

		a -= 1.0f;
		CHECK(a[0] == vec3f{0.0f, 1.0f, 2.0f});
		CHECK(a[1] == vec3f{3.0f, 4.0, 5.0f});
		CHECK(a[2] == vec3f{6.0f, 7.0f, 8.0f});

		a -= vec3f { -1.0f, 2.0f, 0.0f };
		CHECK(a[0] == vec3f{1.0f, -1.0f, 2.0f});
		CHECK(a[1] == vec3f{4.0f, 2.0f, 5.0f});
		CHECK(a[2] == vec3f{7.0f, 5.0f, 8.0f});
	}
}

TEST_CASE("mat3 mul", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat3f a { 1.0f, 2.0f, 3.0f,		//first column
				  4.0f, 5.0f, 6.0f,		//second column
				  7.0f, 8.0f, 9.0f };	//third column

		//our matrices are column major
		mat3f b { 10.0f, 11.0f, 12.0f,		//first column
				  13.0f, 14.0f, 15.0f,		//second column
				  16.0f, 17.0f, 18.0f };	//third column

		mat3f c = a * b;
		CHECK(c[0] == vec3f{dot(a.row(0), b[0]), dot(a.row(1), b[0]), dot(a.row(2), b[0])});
		CHECK(c[1] == vec3f{dot(a.row(0), b[1]), dot(a.row(1), b[1]), dot(a.row(2), b[1])});
		CHECK(c[2] == vec3f{dot(a.row(0), b[2]), dot(a.row(1), b[2]), dot(a.row(2), b[2])});
	}

	SECTION("Case 02")
	{
		//our matrices are column major
		mat3f a { 1.0f, 2.0f, 3.0f,		//first column
				  4.0f, 5.0f, 6.0f,		//second column
				  7.0f, 8.0f, 9.0f };	//third column
		
		vec3f b { 1.0f, 2.0f, 3.0f };
		vec3f c = a * b;
		CHECK(c.x == 30.0f);
		CHECK(c.y == 36.0f);
		CHECK(c.z == 42.0f);
	}
}

TEST_CASE("mat3 transpose", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat3f a { 1.0f, 2.0f, 3.0f,		//first column
				  4.0f, 5.0f, 6.0f,		//second column
				  7.0f, 8.0f, 9.0f };	//third column
		mat3f b = transpose(a);
		CHECK(b[0] == vec3f{1.0f, 4.0f, 7.0f});
		CHECK(b[1] == vec3f{2.0f, 5.0f, 8.0f});
		CHECK(b[2] == vec3f{3.0f, 6.0f, 9.0f});
	}
}

TEST_CASE("mat3 determiniant", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat3f a { 1.0f, 2.0f, 3.0f,		//first column
				  4.0f, 5.0f, 6.0f,		//second column
				  7.0f, 8.0f, 9.0f };	//third column
		CHECK(determinant(a) == 0.0f);
		CHECK(invertible(a) == false);
	}
}

TEST_CASE("mat3 inverse", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat3f a { 1.0f, 2.0f, 3.0f,		//first column
				  4.0f, 5.0f, 6.0f,		//second column
				  7.0f, 8.0f, 10.0f };	//third column
		mat3f b = inverse(a);
		CHECK(b[0] == vec3f { -2.0f/3.0f, -4.0f/3.0f, 1.0f });
		CHECK(b[1] == vec3f { -2.0f/3.0f, 11.0f/3.0f, -2.0f });
		CHECK(b[2] == vec3f { 1.0f, -2.0f, 1.0f });
	}
}


TEST_CASE("mat4 construction", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat4f a { 1.0f, 2.0f, 3.0f, 4.0f,		//first column
				  5.0f, 6.0f, 7.0f, 8.0f,		//second column
				  9.0f, 10.0f, 11.0f, 12.0f,	//third column
				  13.0f, 14.0f, 15.0f, 16.0f };	//fourth column
		CHECK(a[0] == vec4f{1.0f, 2.0f, 3.0f, 4.0f});
		CHECK(a[1] == vec4f{5.0f, 6.0f, 7.0f, 8.0f});
		CHECK(a[2] == vec4f{9.0f, 10.0f, 11.0f, 12.0f});
		CHECK(a[3] == vec4f{13.0f, 14.0f, 15.0f, 16.0f});
	}
}

TEST_CASE("mat4 add", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat4f a { 1.0f, 2.0f, 3.0f, 4.0f,		//first column
				  5.0f, 6.0f, 7.0f, 8.0f,		//second column
				  9.0f, 10.0f, 11.0f, 12.0f,	//third column
				  13.0f, 14.0f, 15.0f, 16.0f };	//fourth column

		a += 1.0f;
		CHECK(a[0] == vec4f{2.0f, 3.0f, 4.0f, 5.0f});
		CHECK(a[1] == vec4f{6.0f, 7.0f, 8.0f, 9.0f});
		CHECK(a[2] == vec4f{10.0f, 11.0f, 12.0f, 13.0f});
		CHECK(a[3] == vec4f{14.0f, 15.0f, 16.0f, 17.0f});

		a += vec4f { -1.0f, 2.0f, 0.0f, 1.0f };
		CHECK(a[0] == vec4f{1.0f, 5.0f, 4.0f, 6.0f});
		CHECK(a[1] == vec4f{5.0f, 9.0f, 8.0f, 10.0f});
		CHECK(a[2] == vec4f{9.0f, 13.0f, 12.0f, 14.0f});
		CHECK(a[3] == vec4f{13.0f, 17.0f, 16.0f, 18.0f});
	}
}

TEST_CASE("mat4 sub", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat4f a { 1.0f, 2.0f, 3.0f, 4.0f,		//first column
				  5.0f, 6.0f, 7.0f, 8.0f,		//second column
				  9.0f, 10.0f, 11.0f, 12.0f,	//third column
				  13.0f, 14.0f, 15.0f, 16.0f };	//fourth column

		a -= 1.0f;
		CHECK(a[0] == vec4f{0.0f, 1.0f, 2.0f, 3.0f});
		CHECK(a[1] == vec4f{4.0f, 5.0f, 6.0f, 7.0f});
		CHECK(a[2] == vec4f{8.0f, 9.0f, 10.0f, 11.0f});
		CHECK(a[3] == vec4f{12.0f, 13.0f, 14.0f, 15.0f});

		a -= vec4f { -1.0f, 2.0f, 0.0f, 1.0f };
		CHECK(a[0] == vec4f{1.0f, -1.0f, 2.0f, 2.0f});
		CHECK(a[1] == vec4f{5.0f, 3.0f, 6.0f, 6.0f});
		CHECK(a[2] == vec4f{9.0f, 7.0f, 10.0f, 10.0f});
		CHECK(a[3] == vec4f{13.0f, 11.0f, 14.0f, 14.0f});
	}
}

TEST_CASE("mat4 mul", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat4f A { 1.0f, 2.0f, 3.0f, 4.0f,		//first column
				  5.0f, 6.0f, 7.0f, 8.0f,		//second column
				  9.0f, 10.0f, 11.0f, 12.0f,	//third column
				  13.0f, 14.0f, 15.0f, 16.0f };	//fourth column

		//our matrices are column major
		mat4f B { 17.0f, 18.0f, 19.0f, 20.0f,		//first column
				  21.0f, 22.0f, 23.0f, 24.0f,		//second column
				  25.0f, 26.0f, 27.0f, 28.0f,	//third column
				  29.0f, 30.0f, 31.0f, 32.0f };	//fourth column

		mat4f C = A * B;

		CHECK(C[0] == vec4f{dot(A.row(0), B[0]), dot(A.row(1), B[0]), dot(A.row(2), B[0]), dot(A.row(3), B[0])});
		CHECK(C[1] == vec4f{dot(A.row(0), B[1]), dot(A.row(1), B[1]), dot(A.row(2), B[1]), dot(A.row(3), B[1])});
		CHECK(C[2] == vec4f{dot(A.row(0), B[2]), dot(A.row(1), B[2]), dot(A.row(2), B[2]), dot(A.row(3), B[2])});
		CHECK(C[3] == vec4f{dot(A.row(0), B[3]), dot(A.row(1), B[3]), dot(A.row(2), B[3]), dot(A.row(3), B[3])});
	}

	SECTION("Case 02")
	{
		//our matrices are column major
		mat4f a { 1.0f, 2.0f, 3.0f, 4.0f,		//first column
				  5.0f, 6.0f, 7.0f, 8.0f,		//second column
				  9.0f, 10.0f, 11.0f, 12.0f,	//third column
				  13.0f, 14.0f, 15.0f, 16.0f };	//fourth column
		
		vec4f b { 1.0f, 2.0f, 3.0f, 4.0f };
		vec4f c = a * b;
		CHECK(c.x == 90.0f);
		CHECK(c.y == 100.0f);
		CHECK(c.z == 110.0f);
		CHECK(c.w == 120.0f);
	}
}

TEST_CASE("mat4 transpose", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat4f a { 1.0f, 2.0f, 3.0f, 4.0f,		//first column
				  5.0f, 6.0f, 7.0f, 8.0f,		//second column
				  9.0f, 10.0f, 11.0f, 12.0f,	//third column
				  13.0f, 14.0f, 15.0f, 16.0f };	//fourth column
		mat4f b = transpose(a);
		CHECK(b[0] == vec4f{1.0f, 5.0f, 9.0f, 13.0f});
		CHECK(b[1] == vec4f{2.0f, 6.0f, 10.0f, 14.0f});
		CHECK(b[2] == vec4f{3.0f, 7.0f, 11.0f, 15.0f});
		CHECK(b[3] == vec4f{4.0f, 8.0f, 12.0f, 16.0f});
	}
}

TEST_CASE("mat4 determiniant", "hml")
{
	SECTION("Case 01")
	{
		//our matrices are column major
		mat4f a { 1.0f, 2.0f, 3.0f, 4.0f,		//first column
				  5.0f, 6.0f, 7.0f, 8.0f,		//second column
				  9.0f, 10.0f, 11.0f, 12.0f,	//third column
				  13.0f, 14.0f, 15.0f, 16.0f };	//fourth column
		CHECK(determinant(a) == 0.0f);
		CHECK(invertible(a) == false);
	}
}

TEST_CASE("mat4 inverse", "hml")
{
	SECTION("Case 01")
	{
		mat4f A{1, 2, 3, 4,
				0, 2, 5, 1,
				2, 0, 0, 1,
				4, 1, 2, 3};

		mat4f I = mat4f::identity();
		mat4f B = inverse(A);
		CHECK(B * A == I);
	}
}

TEST_CASE("quat arithmetic", "hml")
{
	SECTION("Case 01")
	{
		quatf p{};
		quatf q{1.0f, 2.0f, 3.0f, 4.0f};

		quatf r = p + q;
		CHECK(r == quatf{1.0f, 2.0f, 3.0f, 4.0f});
	}

	SECTION("Case 02")
	{
		quatf p = quat_splat<float>(1.0f);
		quatf q{1.0f, 2.0f, 3.0f, 4.0f};

		quatf r = p - q;
		CHECK(r == quatf{0.0f, -1.0f, -2.0f, -3.0f});
	}

	SECTION("Case 03")
	{
		quatf p{1.0f, 2.0f, 3.0f, 4.0f};
		float value = 2.5f;

		auto q = p * value;

		CHECK(q == quatf{2.5f, 5.0f, 7.5f, 10.0f});
	}

	SECTION("Case 04")
	{
		quatf p{1.0f, 2.0f, 3.0f, 4.0f};
		quatf q{5.0f, 6.0f, 7.0f, 8.0f};

		auto r = p * q;

		CHECK(r == quatf{-60.0f, 12.0f, 30.0f, 24.0f});
	}
}

TEST_CASE("quat rotate", "hml")
{
	SECTION("Case 01")
	{
		quatf q = normalize(quatf{1.0f, 0.0f, 1.0f, 0.0f});
		vec4f v { 1.0f, 0.0f, 0.0f, 0.0f };
		vec4f a = quat_to_mat(q) * v;
		vec4f b = q * v;
		CHECK(a[0] == Approx(b[0]).margin(0.000001f));
		CHECK(a[1] == Approx(b[1]).margin(0.000001f));
		CHECK(a[2] == Approx(b[2]).margin(0.000001f));
		CHECK(a[3] == Approx(b[3]).margin(0.000001f));
	}
}

TEST_CASE("quat reciprocal", "hml")
{
	SECTION("Case 01")
	{
		quatf p {1.0f, 2.0f, 3.0f, 4.0f};
		quatf q = reciprocal(p);
		CHECK(q == quatf { 1.0f / 30.0f, -2.0f/30.0f, -3.0f/30.0f, -4.0f/30.0f });
	}
}

TEST_CASE("quat normailze", "hml")
{
	SECTION("Case 01")
	{
		quatf p {1.0f, 2.0f, 3.0f, 4.0f};
		quatf q = normalize(p);
		CHECK(norm_squared(q) == Approx(1.0f));
		CHECK(norm(q) == Approx(1.0f));
	}
}

TEST_CASE("quat conjugate", "hml")
{
	SECTION("Case 01")
	{
		quatf p {1.0f, 2.0f, 3.0f, 4.0f};
		quatf q = conjugate(p);
		CHECK(q == quatf{1.0f, -2.0f, -3.0f, -4.0f});
	}
}
