#pragma once

#include "hml/Flags.h"
#include "hml/Vector.h"
#include "hml/Matrix.h"
#include "hml/Quaternion.h"

namespace hml
{
	constexpr double PI = 3.14159265359;
	constexpr double PI_OVER_2 = 1.57079632679;
	constexpr double TAU = 6.28318530718;
	constexpr vec3f X_AXIS = vec3f{ 1.0f, 0.0f, 0.0f };
	constexpr vec3f Y_AXIS = vec3f{ 0.0f, 1.0f, 0.0f };
	constexpr vec3f Z_AXIS = vec3f{ 0.0f, 0.0f, 1.0f };

	hml_inline static mat4f
	mat4f_scale(const vec3f& v)
	{
		mat4f res = mat4f::identity();
		res[0][0] = v.x;
		res[1][1] = v.y;
		res[2][2] = v.z;
		return res;
	}

	hml_inline static mat4f
	mat4f_translate(const vec3f& v)
	{
		mat4f res = mat4f::identity();
		res[3] = vec4f{ v.x, v.y, v.z, 1.0f };
		return res;
	}

	hml_inline static mat4f
	mat4f_rotate(const quaf& q)
	{
		return to_matrix(q);
	}

	hml_inline static mat4f
	mat4f_transform(const vec3f& scaling, const quatf& rotation, const vec3f& translation)
	{
		return  mat4f_translate(translation) *
				mat4f_rotate(rotation) *
				mat4f_scale(scaling);
	}

	hml_inline static mat4f
	mat4f_viewport(uint32_t width, uint32_t height, float z_near = 0.0f, float z_far = 1.0f)
	{
		mat4f res{};
		//set the scaling part
		res[0][0] = width / 2.0f;
		res[1][1] = height / 2.0f;
		res[2][2] = (z_far - z_near) / 2.0f;

		//set the translation component
		res[3] = vec4f{ width / 2.0f, height / 2.0f, (z_far + z_near) / 2.0f, 1.0f };
		return res;
	}

	hml_inline static mat4f
	mat4f_lookat(const vec3f& eye, const vec3f& target, const vec3f& up)
	{
		//assume a right handed coodinate system

		//forward: vector starting from eye and pointing at target
		const vec3f forward = normalize(target - eye);
		//right: the cross between the forward and the up
		const vec3f right = normalize(cross(forward, up));
		//given the forward and right we need to update the up direction
		const vec3f new_up = cross(right, forward);

		mat4f res = mat4f::identity();

		//right direction: +X Axis
		res[0][0] = right.x;
		res[1][0] = right.y;
		res[2][0] = right.z;

		//up direction: +Y Axis
		res[0][1] = up_new.x;
		res[1][1] = up_new.y;
		res[2][1] = up_new.z;

		//forward direction: -Z Axis
		res[0][2] = -forward.x;
		res[1][2] = -forward.y;
		res[1][2] = -forward.z;

		//translation along each axis which is basically the projection of the eye
		res[3][0] = -dot(right, eye);
		res[3][1] = -dot(up_new, eye);
		res[3][2] = dot(forward, eye);

		return res;
	}

	hml_inline static mat4f
	mat4f_ortho_3d(float left, float right, float bottom, float top, float znear, float zfar)
	{
		mat4f res{};
		//set the scaling part
		res[0][0] = 2 / (right - left);
		res[1][1] = 2 / (top - bottom);
		res[2][2] = -2 / (zfar - znear);

		//set the translation part
		res[3][0] = -(right + left) / (right - left);
		res[3][1] = -(top + bottom) / (top - bottom);
		res[3][2] = -(zfar + znear) / (zfar - znear);

		res[3][3] = 1;
		return res;
	}

	hml_inline static mat4f
	mat4f_ortho_2d(float left, float right, float bottom, float top)
	{
		return mat4f_ortho_3d(left, right, bottom, top, 0.0f, 1.0f);
	}
}
