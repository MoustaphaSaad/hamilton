#pragma once

#include "hml/Flags.h"
#include "hml/Vector.h"
#include "hml/Matrix.h"

namespace hml
{
	template<typename T>
	struct quat
	{
		union {
			vec4<T> data;
			struct
			{
				T w, x, y, z;
			};
		};

		hml_inline T&
		operator[](size_t ix) { return data[ix]; }

		hml_inline const T&
		operator[](size_t ix) const { return data[ix]; }

		hml_inline bool
		operator==(const quat<T>& other) const { return data == other.data; }

		hml_inline bool
		operator!=(const quat<T>& other) const { return !operator==(*this, other); }

		hml_inline T
		real() const { return w; }

		hml_inline vec3<T>
		imaginary() const { return vec3<T> { x, y, z }; }
	};

	//construction
	template<typename T, typename U = T>
	hml_inline static quat<T>
	quat_splat(U c)
	{
		return quat<T>{ vec4_splat<U>(c) };
	}

	template<typename T>
	hml_inline static quat<T>
	quat_from_parts(T real, const vec3<T>& imaginary)
	{
		return quat<T> {
			real,
			imaginary.x,
			imaginary.y,
			imaginary.z
		};
	}

	template<typename T>
	hml_inline static quat<T>
	quat_from_axis(const vec3<T>& axis, T angle)
	{
		const auto real = std::cos(T(0.5) * angle);
		const auto imaginary = normalize(axis) * std::sin(T(0.5) * angle);

		return quat_from_parts(real, imaginary);
	}

	template<typename T>
	hml_inline static quat<T>
	quat_from_angles(T x_angle, T y_angle, T z_angle)
	{
		//left handed clockwise system
		const auto sinx = std::sin(T(0.5) * x_angle);
		const auto cosx = std::cos(T(0.5) * x_angle);

		const auto siny = std::sin(T(0.5) * y_angle);
		const auto cosy = std::cos(T(0.5) * y_angle);

		const auto sinz = std::sin(T(0.5) * z_angle);
		const auto cosz = std::cos(T(0.5) * z_angle);

		return quat<T> {
			cosx * cosy * cosz + sinx * siny * sinz,
			sinx * cosy * cosz - cosx * siny * sinz,
			cosx * siny * cosz + sinx * cosy * sinz,
			cosx * cosy * sinz - sinx * siny * cosz
		};
	}

	template<typename T>
	hml_inline static quat<T>
	quat_from_mat3(const mat3<T>& mat)
	{
		const auto one = T(1.0);
		const auto half = T(0.5);

		T t;
		quat<T> res{};

		if(mat[2][2] < 0)
		{
			if(mat[0][0] > mat[1][1])
			{
				t = one + mat[0][0] - mat[1][1] - mat[2][2];

				res[0] = mat[1][2] - mat[2][1];
				res[1] = t;
				res[2] = mat[0][1] + mat[1][0];
				res[3] = mat[2][0] + mat[0][2];
			}
			else
			{
				t = one - mat[0][0] + mat[1][1] - mat[2][2];

				res[0] = mat[2][0] - mat[2][0];
				res[1] = mat[1][0] + mat[0][1];
				res[2] = t;
				res[3] = mat[1][2] + mat[2][1];
			}
		}
		else
		{
			if(mat[0][0] < -mat[1][1])
			{
				t = one - mat[0][0] - mat[1][1] + mat[2][2];

				res[0] = mat[0][1] - mat[1][0];
				res[1] = mat[2][0] + mat[0][2];
				res[2] = mat[1][2] + mat[2][1];
				res[3] = t;
			}
			else
			{
				t = one + mat[0][0] + mat[1][1] + mat[2][2];

				res[0] = t;
				res[1] = mat[1][2] - mat[2][1];
				res[2] = mat[2][0] - mat[0][2];
				res[3] = mat[0][1] - mat[1][0];
			}
		}

		res.data *= half / std::sqrt(t);
		return res;
	}

	template<typename T>
	hml_inline static quat<T>
	quat_from_mat3(const mat4<T>& mat)
	{
		const auto one = T(1.0);
		const auto half = T(0.5);

		T t;
		quat<T> res{};

		if(mat[2][2] < 0)
		{
			if(mat[0][0] > mat[1][1])
			{
				t = one + mat[0][0] - mat[1][1] - mat[2][2];

				res[0] = mat[1][2] - mat[2][1];
				res[1] = t;
				res[2] = mat[0][1] + mat[1][0];
				res[3] = mat[2][0] + mat[0][2];
			}
			else
			{
				t = one - mat[0][0] + mat[1][1] - mat[2][2];

				res[0] = mat[2][0] - mat[2][0];
				res[1] = mat[1][0] + mat[0][1];
				res[2] = t;
				res[3] = mat[1][2] + mat[2][1];
			}
		}
		else
		{
			if(mat[0][0] < -mat[1][1])
			{
				t = one - mat[0][0] - mat[1][1] + mat[2][2];

				res[0] = mat[0][1] - mat[1][0];
				res[1] = mat[2][0] + mat[0][2];
				res[2] = mat[1][2] + mat[2][1];
				res[3] = t;
			}
			else
			{
				t = one + mat[0][0] + mat[1][1] + mat[2][2];

				res[0] = t;
				res[1] = mat[1][2] - mat[2][1];
				res[2] = mat[2][0] - mat[0][2];
				res[3] = mat[0][1] - mat[1][0];
			}
		}

		res.data *= half / std::sqrt(t);
		return res;
	}

	//operator+
	template<typename T>
	hml_inline static quat<T>
	operator+(const quat<T>& p, const quat<T>& q)
	{
		return quat<T> {
			p.data + q.data
		};
	}

	template<typename T>
	hml_inline static quat<T>&
	operator+=(quat<T>& p, const quat<T>& q)
	{
		p.data += q.data;
		return p;
	}

	template<typename T>
	hml_inline static quat<T>
	operator+(const quat<T>& p, T c)
	{
		return quat<T> {
			p.data + c
		};
	}

	template<typename T>
	hml_inline static quat<T>
	operator+(T c, const quat<T>& p)
	{
		return quat<T> {
			c + p.data
		};
	}

	template<typename T>
	hml_inline static quat<T>&
	operator+=(const quat<T>& p, T c)
	{
		p.data += c;
		return p;
	}


	//operator-
	template<typename T>
	hml_inline static quat<T>
	operator-(const quat<T>& p, const quat<T>& q)
	{
		return quat<T> {
			p.data - q.data
		};
	}

	template<typename T>
	hml_inline static quat<T>&
	operator-=(quat<T>& p, const quat<T>& q)
	{
		p.data -= q.data;
		return p;
	}

	template<typename T>
	hml_inline static quat<T>
	operator-(const quat<T>& p, T c)
	{
		return quat<T> {
			p.data - c
		};
	}

	template<typename T>
	hml_inline static quat<T>
	operator-(T c, const quat<T>& p)
	{
		return quat<T> {
			c - p.data
		};
	}

	template<typename T>
	hml_inline static quat<T>&
	operator-=(quat<T>& p, T c)
	{
		p.data -= c;
		return p;
	}


	//operator*
	template<typename T>
	hml_inline static quat<T>
	operator*(const quat<T>& q, T c)
	{
		return quat<T> {
			q.data * c
		};
	}

	template<typename T>
	hml_inline static quat<T>
	operator*(T c, const quat<T>& q)
	{
		return quat<T> {
			c * q.data
		};
	}

	template<typename T>
	hml_inline static quat<T>&
	operator*=(quat<T>& q, T c)
	{
		q.data *= c;
		return q;
	}

	template<typename T>
	hml_inline static quat<T>
	operator*(const quat<T>& p, const quat<T>& q)
	{
		const auto a1 = p[0], b1 = p[1], c1 = p[2], d1 = p[3];
		const auto a2 = q[0], b2 = q[1], c2 = q[2], d2 = q[3];

		return quat<T> {
			a1 * a2 - b1 * b2 - c1 * c2 - d1 * d2,
			a1 * b2 + b1 * a2 + c1 * d2 - d1 * c2,
			a1 * c2 - b1 * d2 + c1 * a2 + d1 * b2,
			a1 * d2 + b1 * c2 - c1 * b2 + d1 * a2
		};
	}

	template<typename T>
	hml_inline static quat<T>&
	operator*=(quat<T>& p, const quat<T>& q)
	{
		p = p * q;
		return p;
	}

	//rotate
	//https://code.google.com/archive/p/kri/wikis/Quaternions.wiki
	template<typename T>
	hml_inline static vec3<T>
	operator*(const quat<T>& q, const vec3<T>& v)
	{
		T r = q.real();
		vec3<T> i = q.imaginary();
		return i * 2 * dot(i, v) + v * (2 * r * r - 1) + cross(i, v) * 2 * r;
	}

	template<typename T>
	hml_inline static vec4<T>
	operator*(const quat<T>& q, const vec4<T>& vec)
	{
		vec3f res = q * vec3f{vec[0], vec[1], vec[2]};
		return vec4<T> {
			res[0],
			res[1],
			res[2],
			vec[3]
		};
	}

	//operator/
	template<typename T>
	hml_inline static quat<T>
	operator/(const quat<T>& q, T c)
	{
		return quat<T> {
			q.data / c
		};
	}

	template<typename T>
	hml_inline static quat<T>&
	operator/=(quat<T>& q, T c)
	{
		q.data /= c;
		return q;
	}

	template<typename T>
	hml_inline static quat<T>
	operator/(T c, const quat<T>& q)
	{
		return quat<T> {
			c / q.data
		};
	}

	template<typename T>
	hml_inline static quat<T>
	operator/(const quat<T>& p, const quat<T>& q)
	{
		return p * reciprocal(q);
	}

	template<typename T>
	hml_inline static quat<T>&
	operator/=(quat<T>& p, const quat<T>& q)
	{
		p *= q;
		return p;
	}

	//conjugate
	template<typename T>
	hml_inline static quat<T>
	conjugate(const quat<T>& q)
	{
		return quat<T> {
			q[0],
			q[1] * T(-1),
			q[2] * T(-1),
			q[3] * T(-1)
		};
	}

	//norm
	template<typename T>
	hml_inline static T
	norm_squared(const quat<T>& q)
	{
		return q[0] * q[0] +
			   q[1] * q[1] +
			   q[2] * q[2] +
			   q[3] * q[3];
	}

	template<typename T>
	hml_inline static T
	norm(const quat<T>& q)
	{
		return std::sqrt(norm_squared(q));
	}

	//normalize
	template<typename T>
	hml_inline static quat<T>
	normalize(const quat<T>& q)
	{
		return q / norm(q);
	}

	//reciprocal
	template<typename T>
	hml_inline static quat<T>
	reciprocal(const quat<T>& q)
	{
		return conjugate(q) / norm_squared(q);
	}

	//convert to mat4
	template<typename T>
	hml_inline static mat4<T>
	quat_to_mat(const quat<T>& q)
	{
		T w = q[0];
		T x = q[1];
		T y = q[2];
		T z = q[3];

		return mat4<T> {
			1 - 2*y*y - 2*z*z,		2*x*y + 2*z*w,		2*x*z - 2*y*w,		0, //first column
			2*x*y - 2*z*w,			1 - 2*x*x - 2*z*z,	2*y*z + 2*x*w,		0, //second column
			2*x*z + 2*y*w,			2*y*z - 2*x*w,		1 - 2*x*x - 2*y*y,	0, //third column
			0,						0,					0,					1
		};
	}

	//sugar coating
	using quatf = quat<float>;
	using quatd = quat<double>;
}
