#pragma once

#include "hml/Vector.h"

namespace hml
{
	template<typename T>
	struct mat2
	{
		constexpr static size_t D = 2;

		vec2<T> cols[D];

		hml_inline vec2<T>&
		operator[](size_t i)
		{
			return cols[i];
		}

		hml_inline const vec2<T>&
		operator[](size_t i) const
		{
			return cols[i];
		}

		hml_inline vec2<T>
		row(size_t i) const
		{
			return vec2<T> {
				cols[0][i],
				cols[1][i]
			};
		}

		bool
		operator==(const mat2<T>& other) const
		{
			return cols[0] == other.cols[0] && cols[1] == other.cols[1];
		}

		bool
		operator!=(const mat2<T>& other) const
		{
			return !operator==(other);
		}

		inline static mat2<T>
		identity()
		{
			return mat2<T> {
				T(1), T(0),
				T(0), T(1)
			};
		}
	};

	//operator+
	template<typename T>
	hml_inline static mat2<T>
	operator+(const mat2<T>& a, const mat2<T>& b)
	{
		return mat2<T> {
			a[0] + b[0],
			a[1] + b[1]
		};
	}

	template<typename T>
	hml_inline static mat2<T>&
	operator+=(mat2<T>& a, const mat2<T>& b)
	{
		a[0] += b[0];
		a[1] += b[1];
		return a;
	}

	template<typename T>
	hml_inline static mat2<T>
	operator+(const mat2<T>& a, const vec2<T>& b)
	{
		return mat2<T> {
			a[0] + b,
			a[1] + b
		};
	}

	template<typename T>
	hml_inline static mat2<T>&
	operator+=(mat2<T>& a, const vec2<T>& b)
	{
		a[0] += b;
		a[1] += b;
		return a;
	}

	template<typename T>
	hml_inline static mat2<T>
	operator+(const vec2<T>& b, const mat2<T>& a)
	{
		return mat2<T> {
			b + a[0],
			b + a[1]
		};
	}

	template<typename T>
	hml_inline static mat2<T>
	operator+(const mat2<T>& a, T b)
	{
		return mat2<T> {
			a[0] + b,
			a[1] + b
		};
	}

	template<typename T>
	hml_inline static mat2<T>&
	operator+=(mat2<T>& a, T b)
	{
		a[0] += b;
		a[1] += b;
		return a;
	}

	template<typename T>
	hml_inline static mat2<T>
	operator+(T b, const mat2<T>& a)
	{
		return mat2<T> {
			b + a[0],
			b + a[1]
		};
	}

	//operator-
	template<typename T>
	hml_inline static mat2<T>
	operator-(const mat2<T>& a, const mat2<T>& b)
	{
		return mat2<T> {
			a[0] - b[0],
			a[1] - b[1]
		};
	}

	template<typename T>
	hml_inline static mat2<T>&
	operator-=(mat2<T>& a, const mat2<T>& b)
	{
		a[0] -= b[0];
		a[1] -= b[1];
		return a;
	}

	template<typename T>
	hml_inline static mat2<T>
	operator-(const mat2<T>& a, const vec2<T>& b)
	{
		return mat2<T> {
			a[0] - b,
			a[1] - b
		};
	}

	template<typename T>
	hml_inline static mat2<T>&
	operator-=(mat2<T>& a, const vec2<T>& b)
	{
		a[0] -= b;
		a[1] -= b;
		return a;
	}

	template<typename T>
	hml_inline static mat2<T>
	operator-(const vec2<T>& b, const mat2<T>& a)
	{
		return mat2<T> {
			b - a[0],
			b - a[1]
		};
	}

	template<typename T>
	hml_inline static mat2<T>
	operator-(const mat2<T>& a, T b)
	{
		return mat2<T> {
			a[0] - b,
			a[1] - b
		};
	}

	template<typename T>
	hml_inline static mat2<T>&
	operator-=(mat2<T>& a, T b)
	{
		a[0] -= b;
		a[1] -= b;
		return a;
	}

	template<typename T>
	hml_inline static mat2<T>
	operator-(T b, const mat2<T>& a)
	{
		return mat2<T> {
			b - a[0],
			b - a[1]
		};
	}

	//operator*
	template<typename T>
	hml_inline static mat2<T>
	operator*(const mat2<T>& a, const mat2<T>& b)
	{
		return mat2<T> {
			a[0] * b[0][0] + a[1] * b[0][1],
			a[0] * b[1][0] + a[1] * b[1][1]
		};
	}

	template<typename T>
	hml_inline static mat2<T>&
	operator*=(mat2<T>& a, const mat2<T>& b)
	{
		a = a * b;
		return a;
	}

	template<typename T>
	hml_inline static vec2<T>
	operator*(const mat2<T>& a, const vec2<T>& b)
	{
		return (a[0] * b[0] +
				a[1] * b[1]);
	}

	template<typename T>
	hml_inline static mat2<T>
	operator*(const mat2<T>& a, T b)
	{
		return mat2<T> {
			a[0] * b,
			a[1] * b
		};
	}

	template<typename T>
	hml_inline static mat2<T>&
	operator*=(mat2<T>& a, T b)
	{
		a[0] *= b;
		a[1] *= b;
		return a;
	}

	//transpose
	template<typename T>
	hml_inline static mat2<T>
	transpose(const mat2<T>& a)
	{
		return mat2<T> {
			a[0][0], a[1][0],
			a[0][1], a[1][1]
		};
	}

	//determinant
	template<typename T>
	hml_inline static T
	determinant(const mat2<T>& a)
	{
		return a[0][0] * a[1][1] - a[0][1] * a[1][0];
	}

	//invertible
	template<typename T>
	hml_inline static bool
	invertible(const mat2<T>& a)
	{
		return determinant(a) != T(0);
	}

	//inverse
	template<typename T>
	hml_inline static mat2<T>
	inverse(const mat2<T>& a)
	{
		T det = determinant(a);
		assert(det != T(0));

		det = T(1) / det;
		mat2<T> res{};
		res[0][0] = a[1][1] * det;
		res[0][1] = -a[0][1] * det;
		res[1][0] = -a[1][0] * det;
		res[1][1] = a[0][0] * det;
		return res;
	}


	//mat3
	template<typename T>
	struct mat3
	{
		constexpr static size_t D = 3;

		vec3<T> cols[D];

		hml_inline vec3<T>&
		operator[](size_t i)
		{
			return cols[i];
		}

		hml_inline const vec3<T>&
		operator[](size_t i) const
		{
			return cols[i];
		}

		hml_inline vec3<T>
		row(size_t i) const
		{
			return vec3<T> {
				cols[0][i],
				cols[1][i],
				cols[2][i]
			};
		}

		bool
		operator==(const mat3<T>& other) const
		{
			return cols[0] == other.cols[0] && cols[1] == other.cols[1] && cols[2] == other.cols[2];
		}

		bool
		operator!=(const mat3<T>& other) const
		{
			return !operator==(other);
		}

		inline static mat3<T>
		identity()
		{
			return mat3<T> {
				T(1), T(0), T(0),
				T(0), T(1), T(0),
				T(0), T(0), T(1)
			};
		}
	};

	//operator+
	template<typename T>
	hml_inline static mat3<T>
	operator+(const mat3<T>& a, const mat3<T>& b)
	{
		return mat3<T> {
			a[0] + b[0],
			a[1] + b[1],
			a[2] + b[2]
		};
	}

	template<typename T>
	hml_inline static mat3<T>&
	operator+=(mat3<T>& a, const mat3<T>& b)
	{
		a[0] += b[0];
		a[1] += b[1];
		a[2] += b[2];
		return a;
	}

	template<typename T>
	hml_inline static mat3<T>
	operator+(const mat3<T>& a, const vec3<T>& b)
	{
		return mat3<T> {
			a[0] + b,
			a[1] + b,
			a[2] + b
		};
	}

	template<typename T>
	hml_inline static mat3<T>&
	operator+=(mat3<T>& a, const vec3<T>& b)
	{
		a[0] += b;
		a[1] += b;
		a[2] += b;
		return a;
	}

	template<typename T>
	hml_inline static mat3<T>
	operator+(const vec3<T>& b, const mat3<T>& a)
	{
		return mat3<T> {
			b + a[0],
			b + a[1],
			b + a[2]
		};
	}

	template<typename T>
	hml_inline static mat3<T>
	operator+(const mat3<T>& a, T b)
	{
		return mat3<T> {
			a[0] + b,
			a[1] + b,
			a[2] + b
		};
	}

	template<typename T>
	hml_inline static mat3<T>&
	operator+=(mat3<T>& a, T b)
	{
		a[0] += b;
		a[1] += b;
		a[2] += b;
		return a;
	}

	template<typename T>
	hml_inline static mat3<T>
	operator+(T b, const mat3<T>& a)
	{
		return mat3<T> {
			b + a[0],
			b + a[1],
			b + a[2]
		};
	}

	//operator-
	template<typename T>
	hml_inline static mat3<T>
	operator-(const mat3<T>& a, const mat3<T>& b)
	{
		return mat3<T> {
			a[0] - b[0],
			a[1] - b[1],
			a[2] - b[2]
		};
	}

	template<typename T>
	hml_inline static mat3<T>&
	operator-=(mat3<T>& a, const mat3<T>& b)
	{
		a[0] -= b[0];
		a[1] -= b[1];
		a[2] -= b[2];
		return a;
	}

	template<typename T>
	hml_inline static mat3<T>
	operator-(const mat3<T>& a, const vec3<T>& b)
	{
		return mat3<T> {
			a[0] - b,
			a[1] - b,
			a[2] - b
		};
	}

	template<typename T>
	hml_inline static mat3<T>&
	operator-=(mat3<T>& a, const vec3<T>& b)
	{
		a[0] -= b;
		a[1] -= b;
		a[2] -= b;
		return a;
	}

	template<typename T>
	hml_inline static mat3<T>
	operator-(const vec3<T>& b, const mat3<T>& a)
	{
		return mat3<T> {
			b - a[0],
			b - a[1],
			b - a[2]
		};
	}

	template<typename T>
	hml_inline static mat3<T>
	operator-(const mat3<T>& a, T b)
	{
		return mat3<T> {
			a[0] - b,
			a[1] - b,
			a[2] - b
		};
	}

	template<typename T>
	hml_inline static mat3<T>&
	operator-=(mat3<T>& a, T b)
	{
		a[0] -= b;
		a[1] -= b;
		a[2] -= b;
		return a;
	}

	template<typename T>
	hml_inline static mat3<T>
	operator-(T b, const mat3<T>& a)
	{
		return mat3<T> {
			b - a[0],
			b - a[1],
			b - a[2]
		};
	}

	//operator*
	template<typename T>
	hml_inline static mat3<T>
	operator*(const mat3<T>& a, const mat3<T>& b)
	{
		return mat3<T> {
			a[0] * b[0][0] + a[1] * b[0][1] + a[2] * b[0][2],
			a[0] * b[1][0] + a[1] * b[1][1] + a[2] * b[1][2],
			a[0] * b[2][0] + a[1] * b[2][1] + a[2] * b[2][2]
		};
	}

	template<typename T>
	hml_inline static mat3<T>&
	operator*=(mat3<T>& a, const mat3<T>& b)
	{
		a = a * b;
		return a;
	}

	template<typename T>
	hml_inline static vec3<T>
	operator*(const mat3<T>& a, const vec3<T>& b)
	{
		return (a[0] * b[0] +
				a[1] * b[1] +
				a[2] * b[2]);
	}

	template<typename T>
	hml_inline static mat3<T>
	operator*(const mat3<T>& a, T b)
	{
		return mat3<T> {
			a[0] * b,
			a[1] * b,
			a[2] * b
		};
	}

	template<typename T>
	hml_inline static mat3<T>&
	operator*=(mat3<T>& a, T b)
	{
		a[0] *= b;
		a[1] *= b;
		a[2] *= b;
		return a;
	}

	//transpose
	template<typename T>
	hml_inline static mat3<T>
	transpose(const mat3<T>& a)
	{
		return mat3<T> {
			a[0][0], a[1][0], a[2][0],
			a[0][1], a[1][1], a[2][1],
			a[0][2], a[1][2], a[2][2]
		};
	}

	//determinant
	template<typename T>
	hml_inline static T
	determinant(const mat3<T>& a)
	{
		return 	(a[0][0] * a[1][1] * a[2][2]) + (a[1][0] * a[2][1] * a[0][2]) + (a[2][0] * a[0][1] * a[1][2]) -
				(a[0][2] * a[1][1] * a[2][0]) - (a[1][2] * a[2][1] * a[0][0]) - (a[2][2] * a[0][1] * a[1][0]);
	}

	//invertible
	template<typename T>
	hml_inline static bool
	invertible(const mat3<T>& a)
	{
		return determinant(a) != T(0);
	}

	//inverse
	template<typename T>
	hml_inline static mat3<T>
	inverse(const mat3<T>& a)
	{
		T det = determinant(a);
		assert(det != T(0));

		det = T(1) / det;
		mat3<T> res{};
		res[0][0] = det * (a[1][1] * a[2][2] - a[1][2] * a[2][1]);
		res[0][1] = det * (a[2][1] * a[0][2] - a[0][1] * a[2][2]);
		res[0][2] = det * (a[0][1] * a[1][2] - a[0][2] * a[1][1]);

		res[1][0] = det * (a[2][0] * a[1][2] - a[1][0] * a[2][2]);
		res[1][1] = det * (a[0][0] * a[2][2] - a[2][0] * a[0][2]);
		res[1][2] = det * (a[0][2] * a[1][0] - a[0][0] * a[1][2]);

		res[2][0] = det * (a[1][0] * a[2][1] - a[2][0] * a[1][1]);
		res[2][1] = det * (a[0][1] * a[2][0] - a[0][0] * a[2][1]);
		res[2][2] = det * (a[0][0] * a[1][1] - a[0][1] * a[1][0]);
		return res;
	}


	//mat4
	template<typename T>
	struct mat4
	{
		constexpr static size_t D = 4;

		vec4<T> cols[D];

		hml_inline vec4<T>&
		operator[](size_t i)
		{
			return cols[i];
		}

		hml_inline const vec4<T>&
		operator[](size_t i) const
		{
			return cols[i];
		}

		hml_inline vec4<T>
		row(size_t i) const
		{
			return vec4<T> {
				cols[0][i],
				cols[1][i],
				cols[2][i],
				cols[3][i]
			};
		}

		bool
		operator==(const mat4<T>& other) const
		{
			return  cols[0] == other.cols[0] && cols[1] == other.cols[1] &&
					cols[2] == other.cols[2] && cols[3] == other.cols[3];
		}

		bool
		operator!=(const mat4<T>& other) const
		{
			return !operator==(other);
		}

		inline static mat4<T>
		identity()
		{
			return mat4<T> {
				T(1), T(0), T(0), T(0),
				T(0), T(1), T(0), T(0),
				T(0), T(0), T(1), T(0),
				T(0), T(0), T(0), T(1)
			};
		}
	};

	//operator+
	template<typename T>
	hml_inline static mat4<T>
	operator+(const mat4<T>& a, const mat4<T>& b)
	{
		return mat4<T> {
			a[0] + b[0],
			a[1] + b[1],
			a[2] + b[2],
			a[3] + b[3]
		};
	}

	template<typename T>
	hml_inline static mat4<T>&
	operator+=(mat4<T>& a, const mat4<T>& b)
	{
		a[0] += b[0];
		a[1] += b[1];
		a[2] += b[2];
		a[3] += b[3];
		return a;
	}

	template<typename T>
	hml_inline static mat4<T>
	operator+(const mat4<T>& a, const vec4<T>& b)
	{
		return mat4<T> {
			a[0] + b,
			a[1] + b,
			a[2] + b,
			a[3] + b
		};
	}

	template<typename T>
	hml_inline static mat4<T>&
	operator+=(mat4<T>& a, const vec4<T>& b)
	{
		a[0] += b;
		a[1] += b;
		a[2] += b;
		a[3] += b;
		return a;
	}

	template<typename T>
	hml_inline static mat4<T>
	operator+(const vec4<T>& b, const mat4<T>& a)
	{
		return mat4<T> {
			b + a[0],
			b + a[1],
			b + a[2],
			b + a[3]
		};
	}

	template<typename T>
	hml_inline static mat4<T>
	operator+(const mat4<T>& a, T b)
	{
		return mat4<T> {
			a[0] + b,
			a[1] + b,
			a[2] + b,
			a[3] + b
		};
	}

	template<typename T>
	hml_inline static mat4<T>&
	operator+=(mat4<T>& a, T b)
	{
		a[0] += b;
		a[1] += b;
		a[2] += b;
		a[3] += b;
		return a;
	}

	template<typename T>
	hml_inline static mat4<T>
	operator+(T b, const mat4<T>& a)
	{
		return mat4<T> {
			b + a[0],
			b + a[1],
			b + a[2],
			b + a[3]
		};
	}

	//operator-
	template<typename T>
	hml_inline static mat4<T>
	operator-(const mat4<T>& a, const mat4<T>& b)
	{
		return mat4<T> {
			a[0] - b[0],
			a[1] - b[1],
			a[2] - b[2],
			a[3] - b[3]
		};
	}

	template<typename T>
	hml_inline static mat4<T>&
	operator-=(mat4<T>& a, const mat4<T>& b)
	{
		a[0] -= b[0];
		a[1] -= b[1];
		a[2] -= b[2];
		a[3] -= b[3];
		return a;
	}

	template<typename T>
	hml_inline static mat4<T>
	operator-(const mat4<T>& a, const vec4<T>& b)
	{
		return mat4<T> {
			a[0] - b,
			a[1] - b,
			a[2] - b,
			a[3] - b
		};
	}

	template<typename T>
	hml_inline static mat4<T>&
	operator-=(mat4<T>& a, const vec4<T>& b)
	{
		a[0] -= b;
		a[1] -= b;
		a[2] -= b;
		a[3] -= b;
		return a;
	}

	template<typename T>
	hml_inline static mat4<T>
	operator-(const vec4<T>& b, const mat4<T>& a)
	{
		return mat4<T> {
			b - a[0],
			b - a[1],
			b - a[2],
			b - a[3]
		};
	}

	template<typename T>
	hml_inline static mat4<T>
	operator-(const mat4<T>& a, T b)
	{
		return mat4<T> {
			a[0] - b,
			a[1] - b,
			a[2] - b,
			a[3] - b
		};
	}

	template<typename T>
	hml_inline static mat4<T>&
	operator-=(mat4<T>& a, T b)
	{
		a[0] -= b;
		a[1] -= b;
		a[2] -= b;
		a[3] -= b;
		return a;
	}

	template<typename T>
	hml_inline static mat4<T>
	operator-(T b, const mat4<T>& a)
	{
		return mat4<T> {
			b - a[0],
			b - a[1],
			b - a[2],
			b - a[3]
		};
	}

	//operator*
	template<typename T>
	hml_inline static mat4<T>
	operator*(const mat4<T>& a, const mat4<T>& b)
	{
		return mat4<T> {
			a[0] * b[0][0] + a[1] * b[0][1] + a[2] * b[0][2] + a[3] * b[0][3],
			a[0] * b[1][0] + a[1] * b[1][1] + a[2] * b[1][2] + a[3] * b[1][3],
			a[0] * b[2][0] + a[1] * b[2][1] + a[2] * b[2][2] + a[3] * b[2][3],
			a[0] * b[3][0] + a[1] * b[3][1] + a[2] * b[3][2] + a[3] * b[3][3]
		};
	}

	template<typename T>
	hml_inline static mat4<T>&
	operator*=(mat4<T>& a, const mat4<T>& b)
	{
		a = a * b;
		return a;
	}

	template<typename T>
	hml_inline static vec4<T>
	operator*(const mat4<T>& a, const vec4<T>& b)
	{
		return (a[0] * b[0] +
				a[1] * b[1] +
				a[2] * b[2] +
				a[3] * b[3]);
	}

	template<typename T>
	hml_inline static mat4<T>
	operator*(const mat4<T>& a, T b)
	{
		return mat4<T> {
			a[0] * b,
			a[1] * b,
			a[2] * b,
			a[3] * b
		};
	}

	template<typename T>
	hml_inline static mat4<T>&
	operator*=(mat4<T>& a, T b)
	{
		a[0] *= b;
		a[1] *= b;
		a[2] *= b;
		a[3] *= b;
		return a;
	}

	//transpose
	template<typename T>
	hml_inline static mat4<T>
	transpose(const mat4<T>& a)
	{
		return mat4<T> {
			a[0][0], a[1][0], a[2][0], a[3][0],
			a[0][1], a[1][1], a[2][1], a[3][1],
			a[0][2], a[1][2], a[2][2], a[3][2],
			a[0][3], a[1][3], a[2][3], a[3][3]
		};
	}

	//determinant
	template<typename T>
	hml_inline static T
	determinant(const mat4<T>& a)
	{
		return  (a[0][0] * a[1][1] - a[1][0] * a[0][1]) * (a[2][2] * a[3][3] - a[3][2] * a[2][3]) -
				(a[0][0] * a[2][1] - a[2][0] * a[0][1]) * (a[1][2] * a[3][3] - a[3][2] * a[1][3]) +
				(a[0][0] * a[3][1] - a[3][0] * a[0][1]) * (a[1][2] * a[2][3] - a[2][2] * a[1][3]) +
				(a[1][0] * a[2][1] - a[2][0] * a[1][1]) * (a[0][2] * a[3][3] - a[3][2] * a[0][3]) -
				(a[1][0] * a[3][1] - a[3][0] * a[1][1]) * (a[0][2] * a[2][3] - a[2][2] * a[0][3]) +
				(a[2][0] * a[3][1] - a[3][0] * a[2][1]) * (a[0][2] * a[1][3] - a[1][2] * a[0][3]);
	}

	//invertible
	template<typename T>
	hml_inline static bool
	invertible(const mat4<T>& a)
	{
		return determinant(a) != T(0);
	}

	//inverse
	template<typename T>
	hml_inline static mat4<T>
	inverse(const mat4<T>& a)
	{
		T det = determinant(a);
		assert(det != T(0));

		mat4<T> res{};
		res[0][0] = ( a[1][1] * (a[2][2] * a[3][3] - a[2][3] * a[3][2]) + a[1][2] * (a[2][3] * a[3][1] - a[2][1] * a[3][3]) + a[1][3] * (a[2][1] * a[3][2] - a[2][2] * a[3][1]) );
		res[0][1] = ( a[2][1] * (a[0][2] * a[3][3] - a[0][3] * a[3][2]) + a[2][2] * (a[0][3] * a[3][1] - a[0][1] * a[3][3]) + a[2][3] * (a[0][1] * a[3][2] - a[0][2] * a[3][1]) );
		res[0][2] = ( a[3][1] * (a[0][2] * a[1][3] - a[0][3] * a[1][2]) + a[3][2] * (a[0][3] * a[1][1] - a[0][1] * a[1][3]) + a[3][3] * (a[0][1] * a[1][2] - a[0][2] * a[1][1]) );
		res[0][3] = ( a[0][1] * (a[1][3] * a[2][2] - a[1][2] * a[2][3]) + a[0][2] * (a[1][1] * a[2][3] - a[1][3] * a[2][1]) + a[0][3] * (a[1][2] * a[2][1] - a[1][1] * a[2][2]) );

		res[1][0] = ( a[1][2] * (a[2][0] * a[3][3] - a[2][3] * a[3][0]) + a[1][3] * (a[2][2] * a[3][0] - a[2][0] * a[3][2]) + a[1][0] * (a[2][3] * a[3][2] - a[2][2] * a[3][3]) );
		res[1][1] = ( a[2][2] * (a[0][0] * a[3][3] - a[0][3] * a[3][0]) + a[2][3] * (a[0][2] * a[3][0] - a[0][0] * a[3][2]) + a[2][0] * (a[0][3] * a[3][2] - a[0][2] * a[3][3]) );
		res[1][2] = ( a[3][2] * (a[0][0] * a[1][3] - a[0][3] * a[1][0]) + a[3][3] * (a[0][2] * a[1][0] - a[0][0] * a[1][2]) + a[3][0] * (a[0][3] * a[1][2] - a[0][2] * a[1][3]) );
		res[1][3] = ( a[0][2] * (a[1][3] * a[2][0] - a[1][0] * a[2][3]) + a[0][3] * (a[1][0] * a[2][2] - a[1][2] * a[2][0]) + a[0][0] * (a[1][2] * a[2][3] - a[1][3] * a[2][2]) );

		res[2][0] = ( a[1][3] * (a[2][0] * a[3][1] - a[2][1] * a[3][0]) + a[1][0] * (a[2][1] * a[3][3] - a[2][3] * a[3][1]) + a[1][1] * (a[2][3] * a[3][0] - a[2][0] * a[3][3]) );
		res[2][1] = ( a[2][3] * (a[0][0] * a[3][1] - a[0][1] * a[3][0]) + a[2][0] * (a[0][1] * a[3][3] - a[0][3] * a[3][1]) + a[2][1] * (a[0][3] * a[3][0] - a[0][0] * a[3][3]) );
		res[2][2] = ( a[3][3] * (a[0][0] * a[1][1] - a[0][1] * a[1][0]) + a[3][0] * (a[0][1] * a[1][3] - a[0][3] * a[1][1]) + a[3][1] * (a[0][3] * a[1][0] - a[0][0] * a[1][3]) );
		res[2][3] = ( a[0][3] * (a[1][1] * a[2][0] - a[1][0] * a[2][1]) + a[0][0] * (a[1][3] * a[2][1] - a[1][1] * a[2][3]) + a[0][1] * (a[1][0] * a[2][3] - a[1][3] * a[2][0]) );

		res[3][0] = ( a[1][0] * (a[2][2] * a[3][1] - a[2][1] * a[3][2]) + a[1][1] * (a[2][0] * a[3][2] - a[2][2] * a[3][0]) + a[1][2] * (a[2][1] * a[3][0] - a[2][0] * a[3][1]) );
		res[3][1] = ( a[2][0] * (a[0][2] * a[3][1] - a[0][1] * a[3][2]) + a[2][1] * (a[0][0] * a[3][2] - a[0][2] * a[3][0]) + a[2][2] * (a[0][1] * a[3][0] - a[0][0] * a[3][1]) );
		res[3][2] = ( a[3][0] * (a[0][2] * a[1][1] - a[0][1] * a[1][2]) + a[3][1] * (a[0][0] * a[1][2] - a[0][2] * a[1][0]) + a[3][2] * (a[0][1] * a[1][0] - a[0][0] * a[1][1]) );
		res[3][3] = ( a[0][0] * (a[1][1] * a[2][2] - a[1][2] * a[2][1]) + a[0][1] * (a[1][2] * a[2][0] - a[1][0] * a[2][2]) + a[0][2] * (a[1][0] * a[2][1] - a[1][1] * a[2][0]) );

		det = T(1) / det;
		res *= det;
		return res;
	}


	//sugar coating the types
	using mat2f = mat2<float>;
	using mat2i = mat2<int32_t>;

	using mat3f = mat3<float>;
	using mat3i = mat3<int32_t>;

	using mat4f = mat4<float>;
	using mat4i = mat4<int32_t>;
}