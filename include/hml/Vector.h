#pragma once

#include "hml/Flags.h"

#include <cmath>

namespace hml
{
	//vec2
	template<typename T>
	struct vec2
	{
		union {
			T data[2];
			struct
			{
				T x, y;
			};
		};

		hml_inline T&
		operator[](size_t ix) { return data[ix]; }

		hml_inline const T&
		operator[](size_t ix) const { return data[ix]; }

		hml_inline bool
		operator==(const vec2<T>& other) const { return (x == other.x && y == other.y); }

		hml_inline bool
		operator!=(const vec2<T>& other) const { return !operator==(*this, other); }
	};

	//construction
	template<typename T, typename U = T>
	hml_inline static vec2<T>
	vec2_splat(const U& value)
	{
		return vec2<T> { static_cast<T>(value), static_cast<T>(value) };
	}

	//operator+
	template<typename T, typename U>
	hml_inline static vec2<T>
	operator+(const vec2<T>& a, const vec2<U>& b)
	{
		return vec2<T> { static_cast<T>(a.x + b.x),
						 static_cast<T>(a.y + b.y) };
	}

	template<typename T, typename U>
	hml_inline static vec2<T>
	operator+(const vec2<T>& a, const U& b)
	{
		return vec2<T> { static_cast<T>(a.x + b),
						 static_cast<T>(a.y + b) };
	}

	template<typename T, typename U>
	hml_inline static vec2<T>
	operator+(const U& b, const vec2<T>& a)
	{
		return vec2<T> { static_cast<T>(b + a.x),
						 static_cast<T>(b + a.y) };
	}

	//operator+=
	template<typename T, typename U>
	hml_inline static vec2<T>&
	operator+=(vec2<T>& a, const vec2<U>& b)
	{
		a.x = static_cast<T>(a.x + b.x);
		a.y = static_cast<T>(a.y + b.y);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec2<T>&
	operator+=(vec2<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x + b);
		a.y = static_cast<T>(a.y + b);
		return a;
	}

	//operator-
	template<typename T, typename U>
	hml_inline static vec2<T>
	operator-(const vec2<T>& a, const vec2<U>& b)
	{
		return vec2<T> { static_cast<T>(a.x - b.x),
						 static_cast<T>(a.y - b.y) };
	}

	template<typename T, typename U>
	hml_inline static vec2<T>
	operator-(const vec2<T>& a, const U& b)
	{
		return vec2<T> { static_cast<T>(a.x - b),
						 static_cast<T>(a.y - b) };
	}

	template<typename T, typename U>
	hml_inline static vec2<T>
	operator-(const U& b, const vec2<T>& a)
	{
		return vec2<T> { static_cast<T>(b - a.x),
						 static_cast<T>(b - a.y) };
	}

	//operator-=
	template<typename T, typename U>
	hml_inline static vec2<T>&
	operator-=(vec2<T>& a, const vec2<U>& b)
	{
		a.x = static_cast<T>(a.x - b.x);
		a.y = static_cast<T>(a.y - b.y);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec2<T>&
	operator-=(vec2<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x - b);
		a.y = static_cast<T>(a.y - b);
		return a;
	}

	//operator*
	template<typename T, typename U>
	hml_inline static vec2<T>
	operator*(const vec2<T>& a, const vec2<U>& b)
	{
		return vec2<T> { static_cast<T>(a.x * b.x),
						 static_cast<T>(a.y * b.y) };
	}

	template<typename T, typename U>
	hml_inline static vec2<T>
	operator*(const vec2<T>& a, const U& b)
	{
		return vec2<T> { static_cast<T>(a.x * b),
						 static_cast<T>(a.y * b) };
	}

	template<typename T, typename U>
	hml_inline static vec2<T>
	operator*(const U& b, const vec2<T>& a)
	{
		return vec2<T> { static_cast<T>(b * a.x),
						 static_cast<T>(b * a.y) };
	}

	//operator*=
	template<typename T, typename U>
	hml_inline static vec2<T>&
	operator*=(vec2<T>& a, const vec2<U>& b)
	{
		a.x = static_cast<T>(a.x * b.x);
		a.y = static_cast<T>(a.y * b.y);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec2<T>&
	operator*=(vec2<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x * b);
		a.y = static_cast<T>(a.y * b);
		return a;
	}

	//operator/
	template<typename T, typename U>
	hml_inline static vec2<T>
	operator/(const vec2<T>& a, const vec2<U>& b)
	{
		return vec2<T> { static_cast<T>(a.x / b.x),
						 static_cast<T>(a.y / b.y) };
	}

	template<typename T, typename U>
	hml_inline static vec2<T>
	operator/(const vec2<T>& a, const U& b)
	{
		return vec2<T> { static_cast<T>(a.x / b),
						 static_cast<T>(a.y / b) };
	}

	template<typename T, typename U>
	hml_inline static vec2<T>
	operator/(const U& b, const vec2<T>& a)
	{
		return vec2<T> { static_cast<T>(b / a.x),
						 static_cast<T>(b / a.y) };
	}

	//operator/=
	template<typename T, typename U>
	hml_inline static vec2<T>&
	operator/=(vec2<T>& a, const vec2<U>& b)
	{
		a.x = static_cast<T>(a.x / b.x);
		a.y = static_cast<T>(a.y / b.y);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec2<T>&
	operator/=(vec2<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x / b);
		a.y = static_cast<T>(a.y / b);
		return a;
	}

	//operator%
	template<typename T, typename U>
	hml_inline static vec2<T>
	operator%(const vec2<T>& a, const vec2<U>& b)
	{
		return vec2<T> { static_cast<T>(std::fmod(a.x, b.x)),
						 static_cast<T>(std::fmod(a.y, b.y)) };
	}

	template<typename T, typename U>
	hml_inline static vec2<T>
	operator%(const vec2<T>& a, const U& b)
	{
		return vec2<T> { static_cast<T>(std::fmod(a.x, b)),
						 static_cast<T>(std::fmod(a.y, b)) };
	}

	template<typename T, typename U>
	hml_inline static vec2<T>
	operator%(const U& b, const vec2<T>& a)
	{
		return vec2<T> { static_cast<T>(std::fmod(b, a.x)),
						 static_cast<T>(std::fmod(b, a.y)) };
	}

	//operator%=
	template<typename T, typename U>
	hml_inline static vec2<T>&
	operator%=(vec2<T>& a, const vec2<U>& b)
	{
		a.x = static_cast<T>(std::fmod(a.x, b.x));
		a.y = static_cast<T>(std::fmod(a.y, b.y));
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec2<T>&
	operator%=(vec2<T>& a, const U& b)
	{
		a.x = static_cast<T>(std::fmod(a.x, b));
		a.y = static_cast<T>(std::fmod(a.y, b));
		return a;
	}

	//unary operators
	template<typename T>
	hml_inline static vec2<T>
	operator+(const vec2<T>& a)
	{
		return a;
	}

	template<typename T>
	hml_inline static vec2<T>
	operator-(const vec2<T>& a)
	{
		return vec2<T> { -a.x, -a.y };
	}

	//sqrt
	template<typename T>
	hml_inline static vec2<T>
	sqrt(const vec2<T>& a)
	{
		return vec2<T> { std::sqrt(a.x), std::sqrt(a.y) };
	}

	//dot
	template<typename T>
	hml_inline static T
	dot(const vec2<T>& a, const vec2<T>& b)
	{
		return ((a.x * b.x) + (a.y * b.y));
	}

	//clamp
	template<typename T>
	hml_inline static vec2<T>
	clamp(const vec2<T>& a, T low, T high)
	{
		return vec2<T> {
			a.x > high ? high : a.x < low ? low : a.x,
			a.y > high ? high : a.y < low ? low : a.y
		};
	}

	template<typename T>
	hml_inline static vec2<T>
	clamp(const vec2<T>& a, const vec2<T>& low, const vec2<T>& high)
	{
		return vec2<T> {
			a.x > high.x ? high.x : a.x < low.x ? low.x : a.x,
			a.y > high.y ? high.y : a.y < low.y ? low.y : a.y
		};
	}

	//length squared
	template<typename T>
	hml_inline static T
	length_squared(const vec2<T>& a)
	{
		return dot(a, a);
	}

	//length
	template<typename T>
	hml_inline static T
	length(const vec2<T>& a)
	{
		return static_cast<T>(std::sqrt(length_squared(a)));
	}

	//normalize
	template<typename T>
	hml_inline static vec2<T>
	normalize(const vec2<T>& a)
	{
		return a * (T(1) / length(a));
	}

	//min
	template<typename T>
	hml_inline static T
	min(const vec2<T>& a)
	{
		T result = a.x;
		result = result < a.y ? result : a.y;
		return result;
	}

	template<typename T>
	hml_inline static vec2<T>
	min(const vec2<T>& a, const vec2<T>& b)
	{
		return vec2<T> {
			a.x < b.x ? a.x : b.x,
			a.y < b.y ? a.y : b.y
		};
	}

	//max
	template<typename T>
	hml_inline static T
	max(const vec2<T>& a)
	{
		T result = a.x;
		result = result > a.y ? result : a.y;
		return result;
	}

	template<typename T>
	hml_inline static vec2<T>
	max(const vec2<T>& a, const vec2<T>& b)
	{
		return vec2<T> {
			a.x > b.x ? a.x : b.x,
			a.y > b.y ? a.y : b.y
		};
	}

	//abs
	template<typename T>
	hml_inline static vec2<T>
	abs(const vec2<T>& a)
	{
		return vec2<T> {
			std::abs(a.x),
			std::abs(a.y)
		};
	}

	//sum
	template<typename T>
	hml_inline static T
	sum(const vec2<T>& a)
	{
		return a.x + a.y;
	}


	//vec3
	template<typename T>
	struct vec3
	{
		union {
			T data[3];
			struct
			{
				T x, y, z;
			};
		};

		hml_inline T&
		operator[](size_t ix) { return data[ix]; }

		hml_inline const T&
		operator[](size_t ix) const { return data[ix]; }

		hml_inline bool
		operator==(const vec3<T>& other) const { return (x == other.x && y == other.y && z == other.z); }

		hml_inline bool
		operator!=(const vec3<T>& other) const { return !operator==(*this, other); }
	};

	//construction
	template<typename T, typename U = T>
	hml_inline static vec3<T>
	vec3_splat(const U& value)
	{
		return vec3<T> {
			static_cast<T>(value),
			static_cast<T>(value),
			static_cast<T>(value)
		};
	}

	//operator+
	template<typename T, typename U>
	hml_inline static vec3<T>
	operator+(const vec3<T>& a, const vec3<U>& b)
	{
		return vec3<T> {
			static_cast<T>(a.x + b.x),
			static_cast<T>(a.y + b.y),
			static_cast<T>(a.z + b.z)
		};
	}

	template<typename T, typename U>
	hml_inline static vec3<T>
	operator+(const vec3<T>& a, const U& b)
	{
		return vec3<T> {
			static_cast<T>(a.x + b),
			static_cast<T>(a.y + b),
			static_cast<T>(a.z + b)
		};
	}

	template<typename T, typename U>
	hml_inline static vec3<T>
	operator+(const U& b, const vec3<T>& a)
	{
		return vec3<T> {
			static_cast<T>(b + a.x),
			static_cast<T>(b + a.y),
			static_cast<T>(b + a.z)
		};
	}

	//operator+=
	template<typename T, typename U>
	hml_inline static vec3<T>&
	operator+=(vec3<T>& a, const vec3<U>& b)
	{
		a.x = static_cast<T>(a.x + b.x);
		a.y = static_cast<T>(a.y + b.y);
		a.z = static_cast<T>(a.z + b.z);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec3<T>&
	operator+=(vec3<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x + b);
		a.y = static_cast<T>(a.y + b);
		a.z = static_cast<T>(a.z + b);
		return a;
	}

	//operator-
	template<typename T, typename U>
	hml_inline static vec3<T>
	operator-(const vec3<T>& a, const vec3<U>& b)
	{
		return vec3<T> {
			static_cast<T>(a.x - b.x),
			static_cast<T>(a.y - b.y),
			static_cast<T>(a.z - b.z)
		};
	}

	template<typename T, typename U>
	hml_inline static vec3<T>
	operator-(const vec3<T>& a, const U& b)
	{
		return vec3<T> {
			static_cast<T>(a.x - b),
			static_cast<T>(a.y - b),
			static_cast<T>(a.z - b)
		};
	}

	template<typename T, typename U>
	hml_inline static vec3<T>
	operator-(const U& b, const vec3<T>& a)
	{
		return vec3<T> {
			static_cast<T>(b - a.x),
			static_cast<T>(b - a.y),
			static_cast<T>(b - a.z)
		};
	}

	//operator-=
	template<typename T, typename U>
	hml_inline static vec3<T>&
	operator-=(vec3<T>& a, const vec3<U>& b)
	{
		a.x = static_cast<T>(a.x - b.x);
		a.y = static_cast<T>(a.y - b.y);
		a.z = static_cast<T>(a.z - b.z);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec3<T>&
	operator-=(vec3<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x - b);
		a.y = static_cast<T>(a.y - b);
		a.z = static_cast<T>(a.z - b);
		return a;
	}

	//operator*
	template<typename T, typename U>
	hml_inline static vec3<T>
	operator*(const vec3<T>& a, const vec3<U>& b)
	{
		return vec3<T> {
			static_cast<T>(a.x * b.x),
			static_cast<T>(a.y * b.y),
			static_cast<T>(a.z * b.z)
		};
	}

	template<typename T, typename U>
	hml_inline static vec3<T>
	operator*(const vec3<T>& a, const U& b)
	{
		return vec3<T> {
			static_cast<T>(a.x * b),
			static_cast<T>(a.y * b),
			static_cast<T>(a.z * b)
		};
	}

	template<typename T, typename U>
	hml_inline static vec3<T>
	operator*(const U& b, const vec3<T>& a)
	{
		return vec3<T> {
			static_cast<T>(b * a.x),
			static_cast<T>(b * a.y),
			static_cast<T>(b * a.z)
		};
	}

	//operator*=
	template<typename T, typename U>
	hml_inline static vec3<T>&
	operator*=(vec3<T>& a, const vec3<U>& b)
	{
		a.x = static_cast<T>(a.x * b.x);
		a.y = static_cast<T>(a.y * b.y);
		a.z = static_cast<T>(a.z * b.z);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec3<T>&
	operator*=(vec3<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x * b);
		a.y = static_cast<T>(a.y * b);
		a.z = static_cast<T>(a.z * b);
		return a;
	}

	//operator/
	template<typename T, typename U>
	hml_inline static vec3<T>
	operator/(const vec3<T>& a, const vec3<U>& b)
	{
		return vec3<T> {
			static_cast<T>(a.x / b.x),
			static_cast<T>(a.y / b.y),
			static_cast<T>(a.z / b.z)
		};
	}

	template<typename T, typename U>
	hml_inline static vec3<T>
	operator/(const vec3<T>& a, const U& b)
	{
		return vec3<T> {
			static_cast<T>(a.x / b),
			static_cast<T>(a.y / b),
			static_cast<T>(a.z / b)
		};
	}

	template<typename T, typename U>
	hml_inline static vec3<T>
	operator/(const U& b, const vec3<T>& a)
	{
		return vec3<T> {
			static_cast<T>(b / a.x),
			static_cast<T>(b / a.y),
			static_cast<T>(b / a.z)
		};
	}

	//operator/=
	template<typename T, typename U>
	hml_inline static vec3<T>&
	operator/=(vec3<T>& a, const vec3<U>& b)
	{
		a.x = static_cast<T>(a.x / b.x);
		a.y = static_cast<T>(a.y / b.y);
		a.z = static_cast<T>(a.z / b.z);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec3<T>&
	operator/=(vec3<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x / b);
		a.y = static_cast<T>(a.y / b);
		a.z = static_cast<T>(a.z / b);
		return a;
	}

	//operator%
	template<typename T, typename U>
	hml_inline static vec3<T>
	operator%(const vec3<T>& a, const vec3<U>& b)
	{
		return vec3<T> {
			static_cast<T>(std::fmod(a.x, b.x)),
			static_cast<T>(std::fmod(a.y, b.y)),
			static_cast<T>(std::fmod(a.z, b.z))
		};
	}

	template<typename T, typename U>
	hml_inline static vec3<T>
	operator%(const vec3<T>& a, const U& b)
	{
		return vec3<T> {
			static_cast<T>(std::fmod(a.x, b)),
			static_cast<T>(std::fmod(a.y, b)),
			static_cast<T>(std::fmod(a.z, b))
		};
	}

	template<typename T, typename U>
	hml_inline static vec3<T>
	operator%(const U& b, const vec3<T>& a)
	{
		return vec3<T> {
			static_cast<T>(std::fmod(b, a.x)),
			static_cast<T>(std::fmod(b, a.y)),
			static_cast<T>(std::fmod(b, a.z))
		};
	}

	//operator%=
	template<typename T, typename U>
	hml_inline static vec3<T>&
	operator%=(vec3<T>& a, const vec3<U>& b)
	{
		a.x = static_cast<T>(std::fmod(a.x, b.x));
		a.y = static_cast<T>(std::fmod(a.y, b.y));
		a.z = static_cast<T>(std::fmod(a.z, b.z));
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec3<T>&
	operator%=(vec3<T>& a, const U& b)
	{
		a.x = static_cast<T>(std::fmod(a.x, b));
		a.y = static_cast<T>(std::fmod(a.y, b));
		a.z = static_cast<T>(std::fmod(a.z, b));
		return a;
	}

	//unary operators
	template<typename T>
	hml_inline static vec3<T>
	operator+(const vec3<T>& a)
	{
		return a;
	}

	template<typename T>
	hml_inline static vec3<T>
	operator-(const vec3<T>& a)
	{
		return vec3<T> { -a.x, -a.y, -a.z };
	}

	//sqrt
	template<typename T>
	hml_inline static vec3<T>
	sqrt(const vec3<T>& a)
	{
		return vec3<T> {
			std::sqrt(a.x),
			std::sqrt(a.y),
			std::sqrt(a.z)
		};
	}

	//dot
	template<typename T>
	hml_inline static T
	dot(const vec3<T>& a, const vec3<T>& b)
	{
		return ((a.x * b.x) + (a.y * b.y) + (a.z * b.z));
	}

	//cross
	template<typename T>
	hml_inline static vec3<T>
	cross(const vec3<T>& a, const vec3<T>& b)
	{
		return vec3<T> {
			a.y * b.z - a.z * b.y,
			a.z * b.x - a.x * b.z,
			a.x * b.y - a.y * b.x
		};
	}

	//clamp
	template<typename T>
	hml_inline static vec3<T>
	clamp(const vec3<T>& a, T low, T high)
	{
		return vec3<T> {
			a.x > high ? high : a.x < low ? low : a.x,
			a.y > high ? high : a.y < low ? low : a.y,
			a.z > high ? high : a.z < low ? low : a.z
		};
	}

	template<typename T>
	hml_inline static vec3<T>
	clamp(const vec3<T>& a, const vec3<T>& low, const vec3<T>& high)
	{
		return vec3<T> {
			a.x > high.x ? high.x : a.x < low.x ? low.x : a.x,
			a.y > high.y ? high.y : a.y < low.y ? low.y : a.y,
			a.z > high.z ? high.z : a.z < low.z ? low.z : a.z
		};
	}

	//length squared
	template<typename T>
	hml_inline static T
	length_squared(const vec3<T>& a)
	{
		return dot(a, a);
	}

	//length
	template<typename T>
	hml_inline static T
	length(const vec3<T>& a)
	{
		return static_cast<T>(std::sqrt(length_squared(a)));
	}

	//normalize
	template<typename T>
	hml_inline static vec3<T>
	normalize(const vec3<T>& a)
	{
		return a * (T(1) / length(a));
	}

	//min
	template<typename T>
	hml_inline static T
	min(const vec3<T>& a)
	{
		T result = a.x;
		result = result < a.y ? result : a.y;
		result = result < a.z ? result : a.z;
		return result;
	}

	template<typename T>
	hml_inline static vec3<T>
	min(const vec3<T>& a, const vec3<T>& b)
	{
		return vec3<T> {
			a.x < b.x ? a.x : b.x,
			a.y < b.y ? a.y : b.y,
			a.z < b.z ? a.z : b.z
		};
	}

	//max
	template<typename T>
	hml_inline static T
	max(const vec3<T>& a)
	{
		T result = a.x;
		result = result > a.y ? result : a.y;
		result = result > a.z ? result : a.z;
		return result;
	}

	template<typename T>
	hml_inline static vec3<T>
	max(const vec3<T>& a, const vec3<T>& b)
	{
		return vec3<T> {
			a.x > b.x ? a.x : b.x,
			a.y > b.y ? a.y : b.y,
			a.z > b.z ? a.z : b.z
		};
	}

	//abs
	template<typename T>
	hml_inline static vec3<T>
	abs(const vec3<T>& a)
	{
		return vec3<T> {
			std::abs(a.x),
			std::abs(a.y),
			std::abs(a.z)
		};
	}

	//sum
	template<typename T>
	hml_inline static T
	sum(const vec3<T>& a)
	{
		return a.x + a.y + a.z;
	}


	//vec4
	template<typename T>
	struct vec4
	{
		union {
			T data[4];
			struct
			{
				T x, y, z, w;
			};
		};

		hml_inline T&
		operator[](size_t ix) { return data[ix]; }

		hml_inline const T&
		operator[](size_t ix) const { return data[ix]; }

		hml_inline bool
		operator==(const vec4<T>& other) const { return (x == other.x && y == other.y && z == other.z && w == other.w); }

		hml_inline bool
		operator!=(const vec4<T>& other) const { return !operator==(*this, other); }
	};

	//construction
	template<typename T, typename U = T>
	hml_inline static vec4<T>
	vec4_splat(const U& value)
	{
		return vec4<T> {
			static_cast<T>(value),
			static_cast<T>(value),
			static_cast<T>(value),
			static_cast<T>(value)
		};
	}

	//operator+
	template<typename T, typename U>
	hml_inline static vec4<T>
	operator+(const vec4<T>& a, const vec4<U>& b)
	{
		return vec4<T> {
			static_cast<T>(a.x + b.x),
			static_cast<T>(a.y + b.y),
			static_cast<T>(a.z + b.z),
			static_cast<T>(a.w + b.w)
		};
	}

	template<typename T, typename U>
	hml_inline static vec4<T>
	operator+(const vec4<T>& a, const U& b)
	{
		return vec4<T> {
			static_cast<T>(a.x + b),
			static_cast<T>(a.y + b),
			static_cast<T>(a.z + b),
			static_cast<T>(a.w + b)
		};
	}

	template<typename T, typename U>
	hml_inline static vec4<T>
	operator+(const U& b, const vec4<T>& a)
	{
		return vec4<T> {
			static_cast<T>(b + a.x),
			static_cast<T>(b + a.y),
			static_cast<T>(b + a.z),
			static_cast<T>(b + a.w)
		};
	}

	//operator+=
	template<typename T, typename U>
	hml_inline static vec4<T>&
	operator+=(vec4<T>& a, const vec4<U>& b)
	{
		a.x = static_cast<T>(a.x + b.x);
		a.y = static_cast<T>(a.y + b.y);
		a.z = static_cast<T>(a.z + b.z);
		a.w = static_cast<T>(a.w + b.w);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec4<T>&
	operator+=(vec4<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x + b);
		a.y = static_cast<T>(a.y + b);
		a.z = static_cast<T>(a.z + b);
		a.w = static_cast<T>(a.w + b);
		return a;
	}

	//operator-
	template<typename T, typename U>
	hml_inline static vec4<T>
	operator-(const vec4<T>& a, const vec4<U>& b)
	{
		return vec4<T> {
			static_cast<T>(a.x - b.x),
			static_cast<T>(a.y - b.y),
			static_cast<T>(a.z - b.z),
			static_cast<T>(a.w - b.w)
		};
	}

	template<typename T, typename U>
	hml_inline static vec4<T>
	operator-(const vec4<T>& a, const U& b)
	{
		return vec4<T> {
			static_cast<T>(a.x - b),
			static_cast<T>(a.y - b),
			static_cast<T>(a.z - b),
			static_cast<T>(a.w - b)
		};
	}

	template<typename T, typename U>
	hml_inline static vec4<T>
	operator-(const U& b, const vec4<T>& a)
	{
		return vec4<T> {
			static_cast<T>(b - a.x),
			static_cast<T>(b - a.y),
			static_cast<T>(b - a.z),
			static_cast<T>(b - a.w)
		};
	}

	//operator-=
	template<typename T, typename U>
	hml_inline static vec4<T>&
	operator-=(vec4<T>& a, const vec4<U>& b)
	{
		a.x = static_cast<T>(a.x - b.x);
		a.y = static_cast<T>(a.y - b.y);
		a.z = static_cast<T>(a.z - b.z);
		a.w = static_cast<T>(a.w - b.w);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec4<T>&
	operator-=(vec4<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x - b);
		a.y = static_cast<T>(a.y - b);
		a.z = static_cast<T>(a.z - b);
		a.w = static_cast<T>(a.w - b);
		return a;
	}

	//operator*
	template<typename T, typename U>
	hml_inline static vec4<T>
	operator*(const vec4<T>& a, const vec4<U>& b)
	{
		return vec4<T> {
			static_cast<T>(a.x * b.x),
			static_cast<T>(a.y * b.y),
			static_cast<T>(a.z * b.z),
			static_cast<T>(a.w * b.w)
		};
	}

	template<typename T, typename U>
	hml_inline static vec4<T>
	operator*(const vec4<T>& a, const U& b)
	{
		return vec4<T> {
			static_cast<T>(a.x * b),
			static_cast<T>(a.y * b),
			static_cast<T>(a.z * b),
			static_cast<T>(a.w * b)
		};
	}

	template<typename T, typename U>
	hml_inline static vec4<T>
	operator*(const U& b, const vec4<T>& a)
	{
		return vec4<T> {
			static_cast<T>(b * a.x),
			static_cast<T>(b * a.y),
			static_cast<T>(b * a.z),
			static_cast<T>(b * a.w)
		};
	}

	//operator*=
	template<typename T, typename U>
	hml_inline static vec4<T>&
	operator*=(vec4<T>& a, const vec4<U>& b)
	{
		a.x = static_cast<T>(a.x * b.x);
		a.y = static_cast<T>(a.y * b.y);
		a.z = static_cast<T>(a.z * b.z);
		a.w = static_cast<T>(a.w * b.w);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec4<T>&
	operator*=(vec4<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x * b);
		a.y = static_cast<T>(a.y * b);
		a.z = static_cast<T>(a.z * b);
		a.w = static_cast<T>(a.w * b);
		return a;
	}

	//operator/
	template<typename T, typename U>
	hml_inline static vec4<T>
	operator/(const vec4<T>& a, const vec4<U>& b)
	{
		return vec4<T> {
			static_cast<T>(a.x / b.x),
			static_cast<T>(a.y / b.y),
			static_cast<T>(a.z / b.z),
			static_cast<T>(a.w / b.w)
		};
	}

	template<typename T, typename U>
	hml_inline static vec4<T>
	operator/(const vec4<T>& a, const U& b)
	{
		return vec4<T> {
			static_cast<T>(a.x / b),
			static_cast<T>(a.y / b),
			static_cast<T>(a.z / b),
			static_cast<T>(a.w / b)
		};
	}

	template<typename T, typename U>
	hml_inline static vec4<T>
	operator/(const U& b, const vec4<T>& a)
	{
		return vec4<T> {
			static_cast<T>(b / a.x),
			static_cast<T>(b / a.y),
			static_cast<T>(b / a.z),
			static_cast<T>(b / a.w)
		};
	}

	//operator/=
	template<typename T, typename U>
	hml_inline static vec4<T>&
	operator/=(vec4<T>& a, const vec4<U>& b)
	{
		a.x = static_cast<T>(a.x / b.x);
		a.y = static_cast<T>(a.y / b.y);
		a.z = static_cast<T>(a.z / b.z);
		a.w = static_cast<T>(a.w / b.w);
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec4<T>&
	operator/=(vec4<T>& a, const U& b)
	{
		a.x = static_cast<T>(a.x / b);
		a.y = static_cast<T>(a.y / b);
		a.z = static_cast<T>(a.z / b);
		a.w = static_cast<T>(a.w / b);
		return a;
	}

	//operator%
	template<typename T, typename U>
	hml_inline static vec4<T>
	operator%(const vec4<T>& a, const vec4<U>& b)
	{
		return vec4<T> {
			static_cast<T>(std::fmod(a.x, b.x)),
			static_cast<T>(std::fmod(a.y, b.y)),
			static_cast<T>(std::fmod(a.z, b.z)),
			static_cast<T>(std::fmod(a.w, b.w))
		};
	}

	template<typename T, typename U>
	hml_inline static vec4<T>
	operator%(const vec4<T>& a, const U& b)
	{
		return vec4<T> {
			static_cast<T>(std::fmod(a.x, b)),
			static_cast<T>(std::fmod(a.y, b)),
			static_cast<T>(std::fmod(a.z, b)),
			static_cast<T>(std::fmod(a.w, b))
		};
	}

	template<typename T, typename U>
	hml_inline static vec4<T>
	operator%(const U& b, const vec4<T>& a)
	{
		return vec4<T> {
			static_cast<T>(std::fmod(b, a.x)),
			static_cast<T>(std::fmod(b, a.y)),
			static_cast<T>(std::fmod(b, a.z)),
			static_cast<T>(std::fmod(b, a.w))
		};
	}

	//operator%=
	template<typename T, typename U>
	hml_inline static vec4<T>&
	operator%=(vec4<T>& a, const vec4<U>& b)
	{
		a.x = static_cast<T>(std::fmod(a.x, b.x));
		a.y = static_cast<T>(std::fmod(a.y, b.y));
		a.z = static_cast<T>(std::fmod(a.z, b.z));
		a.w = static_cast<T>(std::fmod(a.w, b.w));
		return a;
	}

	template<typename T, typename U>
	hml_inline static vec4<T>&
	operator%=(vec4<T>& a, const U& b)
	{
		a.x = static_cast<T>(std::fmod(a.x, b));
		a.y = static_cast<T>(std::fmod(a.y, b));
		a.z = static_cast<T>(std::fmod(a.z, b));
		a.w = static_cast<T>(std::fmod(a.w, b));
		return a;
	}

	//unary operators
	template<typename T>
	hml_inline static vec4<T>
	operator+(const vec4<T>& a)
	{
		return a;
	}

	template<typename T>
	hml_inline static vec4<T>
	operator-(const vec4<T>& a)
	{
		return vec4<T> { -a.x, -a.y, -a.z, -a.w };
	}

	//sqrt
	template<typename T>
	hml_inline static vec4<T>
	sqrt(const vec4<T>& a)
	{
		return vec4<T> {
			std::sqrt(a.x),
			std::sqrt(a.y),
			std::sqrt(a.z),
			std::sqrt(a.w)
		};
	}

	//dot
	template<typename T>
	hml_inline static T
	dot(const vec4<T>& a, const vec4<T>& b)
	{
		return ((a.x * b.x) + (a.y * b.y) + (a.z * b.z) + (a.w * b.w));
	}

	//clamp
	template<typename T>
	hml_inline static vec4<T>
	clamp(const vec4<T>& a, T low, T high)
	{
		return vec4<T> {
			a.x > high ? high : a.x < low ? low : a.x,
			a.y > high ? high : a.y < low ? low : a.y,
			a.z > high ? high : a.z < low ? low : a.z,
			a.w > high ? high : a.w < low ? low : a.w
		};
	}

	template<typename T>
	hml_inline static vec4<T>
	clamp(const vec4<T>& a, const vec4<T>& low, const vec4<T>& high)
	{
		return vec4<T> {
			a.x > high.x ? high.x : a.x < low.x ? low.x : a.x,
			a.y > high.y ? high.y : a.y < low.y ? low.y : a.y,
			a.z > high.z ? high.z : a.z < low.z ? low.z : a.z,
			a.w > high.w ? high.w : a.w < low.w ? low.w : a.w
		};
	}

	//length squared
	template<typename T>
	hml_inline static T
	length_squared(const vec4<T>& a)
	{
		return dot(a, a);
	}

	//length
	template<typename T>
	hml_inline static T
	length(const vec4<T>& a)
	{
		return static_cast<T>(std::sqrt(length_squared(a)));
	}

	//normalize
	template<typename T>
	hml_inline static vec4<T>
	normalize(const vec4<T>& a)
	{
		return a * (T(1) / length(a));
	}

	//min
	template<typename T>
	hml_inline static T
	min(const vec4<T>& a)
	{
		T result = a.x;
		result = result < a.y ? result : a.y;
		result = result < a.z ? result : a.z;
		result = result < a.w ? result : a.w;
		return result;
	}

	template<typename T>
	hml_inline static vec4<T>
	min(const vec4<T>& a, const vec4<T>& b)
	{
		return vec4<T> {
			a.x < b.x ? a.x : b.x,
			a.y < b.y ? a.y : b.y,
			a.z < b.z ? a.z : b.z,
			a.w < b.w ? a.w : b.w
		};
	}

	//max
	template<typename T>
	hml_inline static T
	max(const vec4<T>& a)
	{
		T result = a.x;
		result = result > a.y ? result : a.y;
		result = result > a.z ? result : a.z;
		result = result > a.w ? result : a.w;
		return result;
	}

	template<typename T>
	hml_inline static vec4<T>
	max(const vec4<T>& a, const vec4<T>& b)
	{
		return vec4<T> {
			a.x > b.x ? a.x : b.x,
			a.y > b.y ? a.y : b.y,
			a.z > b.z ? a.z : b.z,
			a.w > b.w ? a.w : b.w
		};
	}

	//abs
	template<typename T>
	hml_inline static vec4<T>
	abs(const vec4<T>& a)
	{
		return vec4<T> {
			std::abs(a.x),
			std::abs(a.y),
			std::abs(a.z),
			std::abs(a.w)
		};
	}

	//sum
	template<typename T>
	hml_inline static T
	sum(const vec4<T>& a)
	{
		return a.x + a.y + a.z + a.w;
	}

	using vec2f = vec2<float>;
	using vec3f = vec3<float>;
	using vec4f = vec4<float>;

	using vec2i = vec2<int>;
	using vec3i = vec3<int>;
	using vec4i = vec4<int>;
}
