#pragma once

#if defined(__clang__) || defined(__clang_major__) || defined(__clang_minor__) || defined(_clange_patchlevel__)
	#define CC_CLANG 1
#elif defined(__GNUC__) || defined(__GNUC_MINOR__) || defined(__GNUC_PATCHLEVEL__)
	#define CC_GCC 1
#elif defined(_MSC_VER)
	#define CC_MSVC 1
#endif

#if CC_MSVC
	#if ((_MSC_FULL_VER >= 170065501) && (_MSC_VER < 1800)) || (_MSC_FULL_VER >= 180020418)
		#define hml_xcall __vectorcall
	#else
		#define hml_xcall __fastcall
	#endif
	#define hml_inline __forceinline

#elif CC_GCC
	#define hml_xcall 
	#define hml_inline inline __attribute__((__always_inline__))
#elif CC_CLANG
	#define hml_xcall __attribute__((vectorcall))
	#define hml_inline inline __attribute__((__always_inline__))
#endif