project "hml"
	language "C++"
	kind "StaticLib"

	files "include/**.h"

	includedirs "include/"

	--language configuration
	cppdialect "c++17"
	systemversion "latest"

	--linux configuration
	filter "system:linux"
		defines { "OS_LINUX" }

	--windows configuration
	filter "system:windows"
		defines { "OS_WINDOWS" }